/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_FMM_MACROS_H
#define STAPL_BENCHMARKS_FMM_MACROS_H

// Disable a few Intel compiler warnings
#ifdef __INTEL_COMPILER
#pragma warning(disable:161 193 383 444 981 1572 2259)
#endif

// Override assertion
#if ASSERT
#include <cassert>
#else
#define assert(x)
#endif

// Detect SIMD Byte length of architecture
#if __MIC__
/// SIMD byte length of MIC
const int SIMD_BYTES = 64;
#elif __AVX__ | __bgq__
/// SIMD byte length of AVX and BG/Q
const int SIMD_BYTES = 32;
#elif __SSE__ | __bgp__ | __sparc_v9__
/// SIMD byte length of SSE and BG/P
const int SIMD_BYTES = 16;
#else
#error no SIMD
#endif

// Bluegene/Q and K computer don't have single precision arithmetic
#if __bgp__ | __bgq__ | __sparc_v9__
#ifndef FP64
#error Please use FP64 for BG/P, BG/Q, and K computer
#endif
#endif

#endif // STAPL_BENCHMARKS_FMM_MACROS_H
