/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_BENCHMARKS_GAP_BFS_VERIFY_HPP
#define STAPL_BENCHMARKS_GAP_BFS_VERIFY_HPP

#include <stapl/containers/graph/algorithms/execute.hpp>

//////////////////////////////////////////////////////////////////////
/// @brief Neighbor operator for the verify parent graph algorithm
///        for BFS. For every edge in BFS tree, the parent should
///        satsify the following conditions:
///          1. The parent's distance is one less than the child's
///          2. Both the parent and child should be discovered
//////////////////////////////////////////////////////////////////////
class verify_parent_neighbor_op
{
  std::size_t m_level;

public:
  verify_parent_neighbor_op(std::size_t level = 0)
    : m_level(level)
  { }

  using result_type = bool;

  template<typename V>
  bool operator()(V v) const
  {
    bool correct_level =
      v.property().level() == m_level - 1;

    bool both_discovered =
      v.property().level() > 0 && m_level > 0;

    // If both vertices were discovered, but their distance is off
    // by more than 1, then the traversal is incorrect
    if (both_discovered && !correct_level)
      stapl::abort("Parent-child level relationship invalid.");

    return false;
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_level);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Vertex operator for the verify parent graph algorithm
///        for BFS. Each vertex will visit its parent in the BFS tree,
///        sending its own level to check if its parent's level makes sense.
//////////////////////////////////////////////////////////////////////
struct verify_parent_vertex_op
{
  using result_type = bool;

  template<typename V, typename Vis>
  bool operator()(V v, Vis vis) const
  {
    auto level = v.property().level();
    auto parent = v.property().parent();

    // Visit my parent in the BFS tree if it's not myself
    if (parent != v.descriptor())
      vis.visit(parent, verify_parent_neighbor_op{level});

    return false;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Neighbor operator for the verify neighbors graph algorithm
///        for BFS. For every edge in the graph, the two endpoints must
///        satsify the following conditions:
///          1. Their distances differ by at most 1.
///          2. Either both vertices were visited or neither were visited
//////////////////////////////////////////////////////////////////////
class verify_neighbor_neighbor_op
{
  std::size_t m_source_level;

public:
  verify_neighbor_neighbor_op(std::size_t level = 0)
    : m_source_level(level)
  { }

  using result_type = bool;

  template<typename V>
  bool operator()(V v) const
  {
    auto target_level = v.property().level();

    bool levels_differ_by_more_than_one = target_level > m_source_level+1;

    // Either both vertices are discovered or both are undiscovered
    bool same_status =
      (m_source_level == 0 && target_level == 0) ||
      (m_source_level != 0 && target_level != 0);

    if (levels_differ_by_more_than_one || !same_status)
      stapl::abort("Neighbors level relationship invalid.");

    return false;
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_source_level);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Vertex operator for the verify neighbors graph algorithms
///        for BFS. Each vertex will visit its neighbors, sending its
///        own level to check if its neighbors' levels make sense.
//////////////////////////////////////////////////////////////////////
struct verify_neighbor_vertex_op
{
  using result_type = bool;

  template<typename V, typename Vis>
  bool operator()(V v, Vis vis) const
  {
    auto level = v.property().level();
    vis.visit_all_edges(v, verify_neighbor_neighbor_op{level});
    return false;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Verify the results of a BFS tree. Checks to make sure that
///        the discovered levels follow basic sanity checks.
///        Note that if the BFS tree is determined to be invalid, this
///        method will call stapl::abort.
//////////////////////////////////////////////////////////////////////
template<typename G>
bool verify_bfs(G& g)
{
  auto policy = stapl::sgl::build_execution_policy(g).kla(0).build();

  // Verify the relationship between a vertex and its parent in the BFS tree
  stapl::sgl::execute(
    policy, g, verify_parent_vertex_op{}, verify_parent_neighbor_op{});

  // Verify the relationship between a vertex and its neighbors
  stapl::sgl::execute(
    policy, g, verify_neighbor_vertex_op{}, verify_neighbor_neighbor_op{});

  // If control reaches this point without aborting, then verification passed
  return true;
}

#endif
