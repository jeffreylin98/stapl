#include <stapl/containers/graph/graph.hpp>
#include <stapl/containers/graph/algorithms/graph_io.hpp>
#include <stapl/containers/graph/algorithms/properties.hpp>
#include <stapl/containers/graph/algorithms/betweenness_centrality.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/containers/graph/generators/binary_tree.hpp>
#include <stapl/containers/graph/generators/complete.hpp>
#include <stapl/containers/graph/generators/grid.hpp>


// Functor to compare BC value in vertices
struct less_centrality
{
  using result_type = bool;

  template<typename U, typename V>
  result_type operator()(U&& u, V&& v) const
  {
    // Access the property to get the BC value 
    return u.property().BC() < v.property().BC();
  }
};

stapl::exit_code stapl_main(int argc, char* arg[])
{
	// Define a graph by describing their characteristics, including the 
	// property for the algorithm to be used.
	using graph_type = stapl::graph<stapl::DIRECTED,
	                                stapl::MULTIEDGES,
	                				stapl::properties::bc_property>;
	using view_type = stapl::graph_view<graph_type>;

	// Different graph generators to test BC algorithm
	/* auto graph_view = Generate Type of Graph here */
	auto graph_view = stapl::generators::make_binary_tree<view_type>(16, true);

	stapl::betweenness_centrality(graph_view);

	auto v = stapl::max_element(graph_view, less_centrality());

	stapl::do_once([&](){
			/* Print out the vertex ID and it's centrality value */
			});

	return 0;

}

