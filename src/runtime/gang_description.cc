/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/gang_description.hpp>
#include <ostream>

namespace stapl {

namespace runtime {

// Output operator
std::ostream& operator<<(std::ostream& os, gang_description const& gd)
{
  os << "Processes: " << gd.get_num_processes()
     << " { ";
  for (const auto pid : gd.get_processes())
    os << pid << ' ';
  os << "}\n"
     << "Logical: " << gd.get_num_locations() << " { ";
  for (auto i = 0u; i < gd.get_num_locations(); ++i)
    os << i << "->" << gd.get_process_id(i) << ' ';
  os << "}\n"
     << "Grouped: ";
  for (const auto pid : gd.get_processes()) {
    const auto v = gd.get_location_ids(pid);
    os << "{[" << pid << "] ";
    for (auto lid : v)
      os << lid << ' ';
    os << "} ";
  }
  return os << std::endl;
}

} // namespace runtime

} // namespace stapl
