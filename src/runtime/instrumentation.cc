/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/config.hpp>

#ifdef STAPL_RUNTIME_ENABLE_INSTRUMENTATION

# include <stapl/runtime/instrumentation.hpp>
# include <stapl/runtime/system.hpp>
# include <algorithm>
# include <cstdlib>
# include <fstream>
# include <mutex>
# include <sstream>
# include <string>
# include <unordered_map>
# include <vector>
# include <boost/accumulators/accumulators.hpp>
# include <boost/accumulators/statistics.hpp>

# warning "Instrumentation is on. Performance may degrade."

namespace stapl {

namespace runtime {

/// Output filename prefix.
static std::string filename_prefix;

/// Accumulated data.
static std::unordered_map<std::string, unsigned int> acc_data;
static std::mutex                                    acc_data_mtx;

/// Statistical data.
static std::unordered_map<std::string, std::vector<int>> stat_data;
static std::mutex                                        stat_data_mtx;


// Initializes instrumentation
void instrument::initialize(option const& opts)
{
  filename_prefix = opts.get<std::string>("STAPL_INSTRUMENTATION_FILE",
                                          "stapl_instrumentation");
}


// Finalizes instrumentation
void instrument::finalize(void)
{
  if (acc_data.empty() && stat_data.empty())
    return;

  // put all data in a stringstream
  std::ostringstream oss;

  // print accumulated data
  for (auto&& it : acc_data) {
    oss << it->first << ": " << it->second << '\n';
  }

  // print statistical data
  oss << std::endl << "Statistical data:\n";
  for (auto&& it : stat_data) {
    using namespace boost::accumulators;

    oss << it->first << ":\n";

    std::vector<int> const& v = it->second;
    accumulator_set<double, features<tag::mean,tag::min,tag::max>> acc;

    acc = std::for_each(v.begin(), v.end(), acc);

    oss << "\tdata points: " << v.size()                       << '\n'
        << "\tavg: "         << boost::accumulators::mean(acc) << '\n'
        << "\tmin: "         << boost::accumulators::min(acc)  << '\n'
        << "\tmax: "         << boost::accumulators::max(acc)  << '\n';
  }

  // write stringstream to file
  const int pid = getpid();
  std::ostringstream oss_fname;
  oss_fname << filename_prefix << '.' << pid;
  std::ofstream os{oss_fname.str().c_str(), std::ios_base::out};
  os << "# STAPL Instrumentation for pid " << pid << '\n';
  os << oss.str() << std::endl;

  // clear everything
  acc_data.clear();
  stat_data.clear();
}


// Increases the call counter for the given string
void instrument::accumulate(const char* s)
{
  const std::string key{s};

  std::lock_guard<std::mutex> lock{acc_data_mtx};
  ++acc_data[key];
}


// Pushes back the data for the given string
void instrument::push_back(const char* s, int n)
{
  const std::string key{s};

  std::lock_guard<std::mutex> lock{stat_data_mtx};
  std::vector<int>& v = stat_data[key];
  v.push_back(n);
}


// Clears all gathered data
void instrument::clear(void)
{
  std::unique_lock<std::mutex> lock1{acc_data_mtx, std::defer_lock};
  std::unique_lock<std::mutex> lock2{stat_data_mtx, std::defer_lock};
  std::lock(lock1, lock2);
  acc_data.clear();
  stat_data.clear();
}

} // namespace runtime

} // namespace stapl

#endif
