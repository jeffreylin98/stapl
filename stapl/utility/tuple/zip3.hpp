/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_ZIP3_HPP
#define STAPL_UTILITY_TUPLE_ZIP3_HPP

#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/utility.hpp>

namespace stapl {
namespace tuple_ops {

namespace detail {

template<
  typename Tuple1,
  typename Tuple2,
  typename Tuple3,
  typename IdxList = make_index_sequence<tuple_size<Tuple1>::value>>
struct zip3_impl;


template<typename ...Elements1,
         typename ...Elements2,
         typename ...Elements3,
         std::size_t... Indices>
struct zip3_impl<
  tuple<Elements1...>,
  tuple<Elements2...>,
  tuple<Elements3...>,
  index_sequence<Indices...>
>
{
  static auto call(tuple<Elements1...> const& t1,
                   tuple<Elements2...> const& t2,
                   tuple<Elements3...> const& t3)
  STAPL_AUTO_RETURN(
    make_tuple(
      make_tuple(get<Indices>(t1), get<Indices>(t2), get<Indices>(t3))...
    )
  )
};

} // namespace detail


template<typename Tuple1, typename Tuple2, typename Tuple3>
auto zip(Tuple1 const& t1, Tuple2 const& t2, Tuple3 const& t3)
STAPL_AUTO_RETURN((
  detail::zip3_impl<Tuple1, Tuple2, Tuple3>::call(t1, t2, t3)
))

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_ZIP3_HPP
