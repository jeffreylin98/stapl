/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_FOR_EACH_HPP
#define STAPL_UTILITY_TUPLE_FOR_EACH_HPP

#include <stapl/utility/tuple/tuple.hpp>

namespace stapl {
namespace tuple_ops {

namespace detail {

template<std::size_t Num, typename Functor, typename ...Elements>
struct for_each_impl
{
  void operator()(tuple<Elements...> const& elements,
                  Functor const& f) const
  {
    for_each_impl<Num - 1, Functor, Elements...>()(elements, f);

    f(std::get<Num - 1>(elements));
  }

  void operator()(tuple<Elements...>& elements,
                  Functor const& f) const
  {
    for_each_impl<Num - 1, Functor, Elements...>()(elements, f);

    f(std::get<Num - 1>(elements));
  }

};


template<typename Functor, typename ...Elements>
struct for_each_impl<0, Functor, Elements...>
{
  void operator()(tuple<Elements...> const&, Functor const&) const
  { }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Applies a function to every element of a given tuple in order.
///
/// @param elements a tuple
/// @param f        a unary operator
//////////////////////////////////////////////////////////////////////
template<typename Functor, typename ...Elements>
void for_each(tuple<Elements...> const& elements, Functor const& f)
{
  const size_t size = sizeof...(Elements);

  detail::for_each_impl<size, Functor, Elements...>()(elements, f);
}


//////////////////////////////////////////////////////////////////////
/// @copybrief for_each
///
/// @param elements a tuple
/// @param f        a unary operator
//////////////////////////////////////////////////////////////////////
template<typename Functor, typename ...Elements>
void for_each(tuple<Elements...>& elements, Functor const& f)
{
  const size_t size = sizeof...(Elements);

  detail::for_each_impl<size, Functor, Elements...>()(elements, f);
}

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_FOR_EACH_HPP
