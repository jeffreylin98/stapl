/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_POP_BACK_HPP
#define STAPL_UTILITY_TUPLE_POP_BACK_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple_element.hpp>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/utility.hpp>

namespace stapl {
namespace tuple_ops {

namespace result_of {

template <typename Tuple,
          typename IdxList = make_index_sequence<tuple_size<Tuple>::value - 1>>
struct pop_back;


template<typename ...Elements, std::size_t... Indices>
struct pop_back<tuple<Elements...>, index_sequence<Indices...>>
{
  using type = tuple<
                 typename tuple_element<
                   Indices, tuple<Elements...>>::type...>;

  static type call(tuple<Elements...> const& elements)
  {
    return type(std::get<Indices>(elements)...);
  }
};


} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief Returns a new tuple, with the last element of the original
/// removed.
///
/// @param t a tuple
//////////////////////////////////////////////////////////////////////
template<typename Tuple>
auto
pop_back(Tuple const& t)
STAPL_AUTO_RETURN((
  result_of::pop_back<Tuple>::call(t)
))

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_POP_BACK_HPP
