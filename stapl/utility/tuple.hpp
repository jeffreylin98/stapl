/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

//////////////////////////////////////////////////////////////////////
/// @file stapl/utility/tuple.hpp This file wraps the use of std::tuple,
///  and implements several of Boost.Fusion's algorithm calls using
///  std::tuple.  See Boost.Fusion's documentation for the full interface.
///  There are a few additional metafunctions that we implement,
///   documented below.
//////////////////////////////////////////////////////////////////////

#ifndef STAPL_UTILITY_TUPLE_HPP
#define STAPL_UTILITY_TUPLE_HPP

#include <type_traits>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple_mpl_adapt.hpp>
#include <boost/type_traits/integral_constant.hpp>

#include <stapl/utility/utility.hpp>
#include <stapl/utility/integer_sequence.hpp>
#include <stapl/utility/tuple/tuple_element.hpp>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/tuple/apply.hpp>
#include <stapl/utility/tuple/back.hpp>
#include <stapl/utility/tuple/ensure_tuple.hpp>
#include <stapl/utility/tuple/extract_1D.hpp>
#include <stapl/utility/tuple/difference.hpp>
#include <stapl/utility/tuple/discard.hpp>
#include <stapl/utility/tuple/expand_and_copy.hpp>
#include <stapl/utility/tuple/filter.hpp>
#include <stapl/utility/tuple/find_first_index.hpp>
#include <stapl/utility/tuple/fold.hpp>
#include <stapl/utility/tuple/for_each.hpp>
#include <stapl/utility/tuple/from_array.hpp>
#include <stapl/utility/tuple/from_index.hpp>
#include <stapl/utility/tuple/front.hpp>
#include <stapl/utility/tuple/ignore_index.hpp>
#include <stapl/utility/tuple/homogeneous_tuple.hpp>
#include <stapl/utility/tuple/pad_tuple.hpp>
#include <stapl/utility/tuple/pop_back.hpp>
#include <stapl/utility/tuple/pop_front.hpp>
#include <stapl/utility/tuple/print.hpp>
#include <stapl/utility/tuple/push_back.hpp>
#include <stapl/utility/tuple/push_front.hpp>
#include <stapl/utility/tuple/rearrange.hpp>
#include <stapl/utility/tuple/reverse.hpp>
#include <stapl/utility/tuple/transform.hpp>
#include <stapl/utility/tuple/tuple_contains.hpp>
#include <stapl/utility/tuple/to_index.hpp>
#include <stapl/utility/tuple/zip3.hpp>

#ifdef _STAPL
#include <stapl/runtime/serialization/tuple.hpp>
#endif

#endif // STAPL_UTILITY_TUPLE_HPP
