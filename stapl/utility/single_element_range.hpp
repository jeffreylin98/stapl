/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_UTILITY_SINGLE_ELEMENT_RANGE_HPP
#define STAPL_UTILITY_SINGLE_ELEMENT_RANGE_HPP

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief A range over a single element that can be used in a range-based
///        for loop.
///
/// @tparam T The type of the single value
//////////////////////////////////////////////////////////////////////
template<typename T>
class single_element_range
{
public:
  using value_type = T;
  using iterator   = value_type*;

private:
  value_type& m_value;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Construct the range by copying the single element
  //////////////////////////////////////////////////////////////////////
  single_element_range(T& value)
    : m_value(value)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Iterator to the first (and only) value in the range
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    return &m_value;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief End iterator (one past the end) for the single value
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return &m_value + 1;
  }
};

} // namespace stapl

#endif
