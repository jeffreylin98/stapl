/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_DOWN_CAST_HPP
#define STAPL_UTILITY_DOWN_CAST_HPP

#include <stapl/runtime/stapl_assert.hpp>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Perform downcast of a pointer from a base class to a derived class.
///   In debug mode, use dynamic_cast and test with assertion.  Otherwise, use
///   static_cast<>().
/// @ingroup utility
//////////////////////////////////////////////////////////////////////
template<typename DerivedPtr, typename Base>
inline DerivedPtr down_cast(Base* const base_ptr)
{
  static_assert(std::is_pointer<DerivedPtr>::value,
                "invalid down_cast conversion");

#ifndef STAPL_NDEBUG
  if (base_ptr == nullptr)
    return nullptr;

  DerivedPtr const derived_ptr = dynamic_cast<DerivedPtr>(base_ptr);

  stapl_assert(derived_ptr != nullptr,
    "dynamic_cast downcast to base_ptr failed");
  return derived_ptr;
#else
  return static_cast<DerivedPtr>(base_ptr);
#endif
}


//////////////////////////////////////////////////////////////////////
/// @brief Perform downcast of a reference from a base class to a derived class.
///   In debug mode, use dynamic_cast and test with assertion.  Otherwise, use
///   static_cast<>().
/// @ingroup utility
//////////////////////////////////////////////////////////////////////
template<typename Derived, typename Base>
inline Derived& down_cast(Base& base_ref)
{
#ifndef STAPL_NDEBUG
  return dynamic_cast<Derived&>(base_ref);
#else
  return static_cast<Derived&>(base_ref);
#endif
}


//////////////////////////////////////////////////////////////////////
/// @brief Perform downcast of a const reference from a base class to a derived
///   class In debug mode, use dynamic_cast and test with assertion.  Otherwise,
///   use static_cast<>().
/// @ingroup utility
//////////////////////////////////////////////////////////////////////
template<typename Derived, typename Base>
inline Derived const& down_cast(Base const& base_ref)
{
#ifndef STAPL_NDEBUG
  return dynamic_cast<Derived const&>(base_ref);
#else
  return static_cast<Derived const&>(base_ref);
#endif
}

} // namespace stapl

#endif // STAPL_UTILITY_DOWN_CAST_HPP
