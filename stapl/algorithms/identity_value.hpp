/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_IDENTITY_VALUE_HPP
#define STAPL_IDENTITY_VALUE_HPP

#include <boost/static_assert.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief  Reflects the identity value for the given operation and operand
///   types.
/// @tparam Operator The operation type for which the identity value is valid.
/// @tparam Operand The operand type from whose domain the identity value is
///   taken.
///
/// Primary template unconditionally statically asserts, as means a valid
/// specialization for the Operator/Operand pair is not defined.
/// @ingroup functionObjects
//////////////////////////////////////////////////////////////////////
template<typename Operator, typename Operand>
struct identity_value
{
  BOOST_STATIC_ASSERT_MSG(
    sizeof(Operator) == 0,
    "Need to define identity_value specialization for operator / operand pair."
  );
};


//////////////////////////////////////////////////////////////////////
/// @brief Macro that reduces boilerplate code for defining identity values
///   when it's explicit specialization (i.e., no partial specialization of
///   either the Operator or Operand parameters.
//////////////////////////////////////////////////////////////////////
#define STAPL_DEFINE_IDENTITY_VALUE(operation, operand, v) \
  template<>                                               \
  struct identity_value<operation, operand>                \
  {                                                        \
    static operand value(void) { return v; }               \
  };

} // namespace stapl

#endif
