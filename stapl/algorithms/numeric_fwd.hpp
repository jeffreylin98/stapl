/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_ALGORITHMS_NUMERIC_FWD_HPP
#define STAPL_ALGORITHMS_NUMERIC_FWD_HPP

#include <stapl/views/view_traits.hpp>

namespace stapl {

template<typename View, typename Oper>
typename view_traits<View>::value_type
accumulate(View const& view,
           typename view_traits<View>::value_type init, Oper oper);

template<typename View>
typename view_traits<View>::value_type
accumulate(View const& view, typename view_traits<View>::value_type init);

template<typename View1, typename View2, typename Oper>
inline
void adjacent_difference(View1 const& view1, View2& view2, Oper oper);

template<typename View1, typename View2>
inline
void adjacent_difference(View1 const& view1, View2& view2);

template<typename View1, typename View2, typename Sum,
         typename Product>
inline
typename view_traits<View1>::value_type
inner_product(View1 const& view1, View2 const& view2,
              typename view_traits<View1>::value_type init, Sum op1,
              Product op2);

template<typename View1, typename View2>
inline
typename view_traits<View1>::value_type
inner_product(View1 const& view1, View2 const& view2,
              typename view_traits<View1>::value_type init);

template <typename View1, typename View2, typename View3,
          typename Sum, typename Product>
typename view_traits<View1>::value_type
weighted_inner_product(View1 const& view1, View2 const& view2, View3 const& wt,
                       typename view_traits<View1>::value_type init,
                       Sum op1, Product op2);

template <typename View1, typename View2, typename View3>
typename view_traits<View1>::value_type
weighted_inner_product(View1 const& view1, View2 const& view2, View3 const& wt,
                       typename view_traits<View1>::value_type init);

template <typename View1, typename View2, typename Sum, typename Product>
typename View1::value_type
weighted_norm(View1 const& view1, View2 const& wt, Sum op1, Product op2);

template <typename View1, typename View2>
typename View1::value_type
weighted_norm(View1 const& view1, View2 const& wt);

template<typename View0, typename View1, typename BinaryFunction>
void
partial_sum(View0 const& view0, View1 const& view1, BinaryFunction binary_op,
            const bool shift = false);


template<typename View0, typename View1>
void
partial_sum(View0 const& view0, View1 const& view1, const bool shift = false);

template <typename View0, typename View1, typename BinaryFunction>
typename View0::value_type
partial_sum_accumulate(View0 const& view0,
                       View1 const& view1,
                       typename view_traits<View0>::value_type init,
                       BinaryFunction binary_op,
                       const bool shift = false);

template <typename View0, typename View1>
typename View0::value_type
partial_sum_accumulate(View0 const& view0,
                       View1 const& view1,
                       typename view_traits<View0>::value_type init,
                       const bool shift = false);

template<typename View0>
void iota(View0 const& view, typename View0::value_type const& value);

} // namespace stapl
#endif
