/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_STATIC_ARRAY_TRAITS_HPP
#define STAPL_CONTAINERS_STATIC_ARRAY_TRAITS_HPP

#include <stapl/containers/distribution/distribution.hpp>
#include <stapl/containers/distribution/operations/base.hpp>
#include <stapl/containers/distribution/operations/iterable.hpp>
#include <stapl/containers/distribution/operations/random_access.hpp>

#include <stapl/containers/distribution/container_manager/container_manager_static.hpp>
#include <stapl/containers/distribution/directory/static_registry.hpp>
#include <stapl/containers/distribution/directory/container_directory.hpp>
#include <stapl/containers/distribution/directory/manager.hpp>
#include <stapl/containers/mapping/mapper.hpp>

#include <stapl/containers/array/base_container.hpp>
#include <stapl/containers/array/base_container_traits.hpp>

#include <stapl/domains/indexed.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Default traits for the static_array container. Specifies
/// customizable type parameters that could be changed on a per-container
/// basis.
/// @ingroup parrayTraits
///
/// @tparam T Type of the stored elements in the array.
/// @see static_array
////////////////////////////////////////////////////////////////////////
template<typename T, typename... OptionalNoInitParam>
struct static_array_traits
{
  using value_type      = T;
  using partition_type  = balanced_partition<indexed_domain<size_t>>;
  using mapper_type     = mapper<size_t>;
  using domain_type     = indexed_domain<size_t>;
  using index_type      = typename domain_type::index_type;
  using gid_type        = index_type;
  using manager_type    = basic_manager;

  using directory_type  =
    container_directory<
      partition_type, mapper_type, manager_type, static_registry<manager_type>>;

  using base_container_type    =
    basic_array_base_container<T, OptionalNoInitParam...>;

  using container_manager_type = container_manager_static<base_container_type>;

  //////////////////////////////////////////////////////////////////////
  /// @brief Metafunction to compute the distribution type based on a
  /// container type.
  /// @tparam C Type of the container.
  //////////////////////////////////////////////////////////////////////
  template <typename C>
  struct construct_distribution
  {
    using type =
     distribution<
       C, operations::base, operations::iterable, operations::random_access>;
  };
}; // struct static_array_traits

} // namespace stapl

#endif // STAPL_CONTAINERS_STATIC_ARRAY_TRAITS_HPP
