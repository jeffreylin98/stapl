/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_INDEX_BOUNDS_HPP
#define STAPL_CONTAINERS_INDEX_BOUNDS_HPP

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/limits.hpp>
#include <string>
#include <utility>
#include <stapl/utility/tuple.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Class to determine the bounds (min, max, error) of an index
/// or GID.
///
/// @tparam T The index type. Typically a container GID or view index_type.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct index_bounds
{
  //////////////////////////////////////////////////////////////////////
  /// @brief Return the lowest value for an index. If the index is integral,
  /// this is equivalent to @ref std::numeric_limits<T>::min(). If T is a float,
  /// this will be @p -std::numeric_limits<T>::max().
  //////////////////////////////////////////////////////////////////////
  static T lowest(void)
  { return boost::numeric::bounds<T>::lowest(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Return the highest value for an index.
  /// This is equivalent to @ref std::numeric_limits<T>::max().
  //////////////////////////////////////////////////////////////////////
  static T highest(void)
  { return boost::numeric::bounds<T>::highest(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Return the smallest positive normalized value for an index.
  /// If the index is integral, this will be 0.
  //////////////////////////////////////////////////////////////////////
  static T smallest(void)
  { return boost::numeric::bounds<T>::smallest(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Return a designated "invalid" value for a GID.
  //////////////////////////////////////////////////////////////////////
  static T invalid(void)
  { return stapl::index_bounds<T>::highest(); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization when the index or GID is a @ref std::string.
///
/// @see index_bounds
//////////////////////////////////////////////////////////////////////
template<>
struct index_bounds<std::string>
{
  static std::string invalid(void)
  { return std::string("STAPL_INVALID_STRING_GID_MUST_BE_UNIQUE_KEY"); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization when the index or GID is a @ref std::pair of the
/// same type.
///
/// @see index_bounds
//////////////////////////////////////////////////////////////////////
template<typename T>
struct index_bounds<std::pair<T,T> >
{
  static std::pair<T,T> invalid(void)
  {
    return std::make_pair(index_bounds<T>::highest(),
                          index_bounds<T>::highest());
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization when the index or GID is a @ref std::pair of
/// different types.
///
/// @see index_bounds
//////////////////////////////////////////////////////////////////////
template<typename... T>
struct index_bounds<stapl::tuple<T...> >
{
  static stapl::tuple<T...> lowest(void)
  {
    return stapl::make_tuple(index_bounds<T>::lowest()...);
  }

  static stapl::tuple<T...> highest(void)
  {
    return stapl::make_tuple(index_bounds<T>::highest()...);
  }

  static stapl::tuple<T...> invalid(void)
  {
    return stapl::make_tuple(index_bounds<T>::highest()...);
  }
};

} // namespace stapl

#endif // STAPL_CONTAINERS_INDEX_BOUNDS_HPP
