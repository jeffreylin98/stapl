/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_HPP

#include <stapl/utility/tuple.hpp>
#include <stapl/views/system_view.hpp>
#include "specifications_fwd.hpp"
#include <stapl/views/array_view.hpp>
#include "specification_functors.hpp"

namespace stapl {

inline distribution_spec<>
block(unsigned long int n, unsigned long int block_size, level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  const unsigned long int num_blocks = n/block_size + (n%block_size ? 1 : 0);

  auto map_factory =
    [num_blocks](location_type num_locs)
    {
      const unsigned long int blocks_per_loc =
        num_blocks/num_locs + (num_blocks%num_locs ? 1 : 0);

      return block_map<id_type, location_type>(blocks_per_loc);
    };

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(num_blocks),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      deferred_map<block_map<id_type, location_type>,
                   decltype(map_factory), id_type, id_type>(map_factory),
      distribution_type::blocked),
    indexed_domain<id_type>(n),
    block_map<id_type, id_type>(block_size), distribution_type::blocked);
}


inline distribution_spec<>
block(unsigned long int n, unsigned long int block_size,
      std::vector<location_type> const& locs)
{
 using id_type           = unsigned long int;
 using mapping_view_type =
   dist_spec_impl::distribution_spec<>::mapping_view_type;

  const unsigned long int num_blocks =
    n / block_size + (n % block_size ? 1 : 0);

  const unsigned long int blocks_per_loc =
    num_blocks / locs.size() + (num_blocks % locs.size() ? 1 : 0);

  return distribution_spec<>(
    new mapping_view_type(
      system_view(locs),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(num_blocks),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      block_map<id_type, location_type>(blocks_per_loc),
      distribution_type::blocked),
    indexed_domain<id_type>(n),
    block_map<id_type, id_type>(block_size), distribution_type::blocked);
}


inline distribution_spec<>
cyclic(unsigned long int n, level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  auto map_factory =
    [](location_type num_locs)
      { return cycle_map<id_type, location_type>(num_locs); };

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(n),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      deferred_map<cycle_map<id_type, location_type>,
                   decltype(map_factory), id_type, id_type>(map_factory),
      distribution_type::cyclical),
    indexed_domain<id_type>(n),
    identity_map<id_type, id_type>(), distribution_type::cyclical);
}


inline distribution_spec<>
cyclic(unsigned long int n,
       std::vector<location_type> const& locs)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  return distribution_spec<>(
    new mapping_view_type(
      system_view(locs),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(n),
        [](indexed_domain<id_type> const& dom, location_type)
          -> indexed_domain<id_type>
        { return dom; }
      ),
      cycle_map<id_type, location_type>(locs.size()),
      distribution_type::cyclical),
    indexed_domain<id_type>(n),
    identity_map<id_type, id_type>(), distribution_type::cyclical);
}


inline distribution_spec<>
block_cyclic(unsigned long int n, unsigned long int block_size, level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  auto map_factory =
    [](location_type num_locs)
      { return cycle_map<id_type, location_type>(num_locs); };

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(n/block_size + (n%block_size?1:0)),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      deferred_map<cycle_map<id_type, location_type>,
                   decltype(map_factory), id_type, id_type>(map_factory),
      distribution_type::block_cyclical),
    indexed_domain<id_type>(n),
    block_map<id_type, id_type>(block_size),
    distribution_type::block_cyclical);
}


inline distribution_spec<>
block_cyclic(unsigned long int n, unsigned long int block_size,
             std::vector<location_type> const& locs)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  return
    distribution_spec<>(
      new mapping_view_type(
        system_view(locs),
        deferred_domain<indexed_domain<id_type>>(
          indexed_domain<id_type>(n/block_size + (n%block_size?1:0)),
          [](indexed_domain<id_type> const& dom, location_type)
            { return dom; }),
        cycle_map<id_type, location_type>(locs.size()),
        distribution_type::block_cyclical),
      indexed_domain<id_type>(n),
      block_map<id_type, id_type>(block_size),
      distribution_type::block_cyclical);
}

namespace detail {
template<typename GID, typename Index>
struct balance_map_factory
{
protected:
  unsigned long int m_n;
public:
  balance_map_factory(unsigned long int n)
    : m_n(n)
  { }

  balance_map<1, GID, Index> operator()(location_type num_locs)
  { return balance_map<1, GID, Index>(m_n, num_locs); }

  void define_type(typer& t)
  { t.member(m_n); }
};
}

inline distribution_spec<>
balance(unsigned long int n, level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  detail::balance_map_factory<id_type, id_type> map_factory(n);

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(n),
        [](indexed_domain<id_type> const& dom, location_type num_locs)
          {
            return dom.size() > num_locs ?
                indexed_domain<id_type>(num_locs)
              : dom;
          }),
      identity_map<id_type, location_type>(), distribution_type::balanced),
    indexed_domain<id_type>(n),
    deferred_map<balance_map<1, id_type, id_type>,
                 detail::balance_map_factory<id_type, id_type>>(
      map_factory), distribution_type::balanced);
}


inline distribution_spec<>
balance(unsigned long int n, std::vector<location_type> const& locs)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  const auto locs_size = locs.size();

  auto map_factory =
    [n, locs_size](location_type)
      { return balance_map<1, id_type, id_type>(n, locs_size); };

  return distribution_spec<>(
    new mapping_view_type(
      system_view(locs),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(n > locs.size() ? locs.size() : n),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      identity_map<id_type, location_type>(), distribution_type::balanced),
    indexed_domain<id_type>(n),
    deferred_map<balance_map<1, id_type, id_type>, decltype(map_factory)>(
      map_factory), distribution_type::balanced);
}


template <typename View>
distribution_spec<>
arbitrary(View const& part_view, level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  std::pair<id_type, id_type> min_max_gids =
    map_reduce(dist_spec_impl::extract_min_max_gid(),
               dist_spec_impl::combine_min_max_gid(),
               part_view);

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(part_view.size()),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      dist_spec_impl::cb_part_to_loc<View>(part_view), true),
    indexed_domain<id_type>(min_max_gids.first, min_max_gids.second, true),
    dist_spec_impl::cb_gid_to_part<View>(part_view), true);
}


template <typename View>
distribution_spec<>
arbitrary(View const& part_view, std::vector<location_type> const& locs)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  std::pair<id_type, id_type> min_max_gids =
    map_reduce(dist_spec_impl::extract_min_max_gid(),
               dist_spec_impl::combine_min_max_gid(),
               part_view);

  return distribution_spec<>(
    new mapping_view_type(
      system_view(locs),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(part_view.size()),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      dist_spec_impl::cb_part_to_loc<View>(part_view), true),
    indexed_domain<id_type>(min_max_gids.first, min_max_gids.second, true),
    dist_spec_impl::cb_gid_to_part<View>(part_view), true);
}


template <typename GIDMapFunc, typename PIDMapFunc>
distribution_spec<>
arbitrary(unsigned long int n, unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  return distribution_spec<>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(nparts),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      pid_to_lid, true),
    indexed_domain<id_type>(n),
    gid_to_pid, true);
}


template <typename Dom, typename GIDMapFunc, typename PIDMapFunc, typename>
distribution_spec<Dom>
arbitrary(Dom const& dom, unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          level lvl)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    typename dist_spec_impl::distribution_spec<Dom>::mapping_view_type;

  return distribution_spec<Dom>(
    new mapping_view_type(
      system_view(lvl),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(nparts),
        [](indexed_domain<id_type> const& loc_dom, location_type)
          { return loc_dom; }
      ),
      pid_to_lid, true),
    dom,
    gid_to_pid, true);
}


template <typename GIDMapFunc, typename PIDMapFunc>
distribution_spec<>
arbitrary(unsigned long int n, unsigned long int nparts,
          GIDMapFunc const &gid_to_pid,
          PIDMapFunc const &pid_to_lid,
          std::vector<location_type> const& locs)
{
  using id_type           = unsigned long int;
  using mapping_view_type =
    dist_spec_impl::distribution_spec<>::mapping_view_type;

  return distribution_spec<>(
    new mapping_view_type(
      system_view(locs),
      deferred_domain<indexed_domain<id_type>>(
        indexed_domain<id_type>(nparts),
        [](indexed_domain<id_type> const& dom, location_type)
          { return dom; }),
      pid_to_lid, true),
    indexed_domain<id_type>(n),
    gid_to_pid, true);
}

} // namespace stapl

#endif // STAPL_CONTAINERS_DISTRIBUTION_SPECIFICATIONS_HPP
