/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_HPP

#include <stapl/views/system_view.hpp>
#include "is_distribution_view_fwd.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if a type is an instantiation of a view-based
///   distribution specification.
//////////////////////////////////////////////////////////////////////
template <typename V, bool is_vw>
struct is_distribution_view
  : public std::false_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if a type is an instantiation of a view-based
///   distribution specification.
///
/// Specialization for the case where the type is a view
//////////////////////////////////////////////////////////////////////
template <typename V>
struct is_distribution_view<V, true>
  : public is_distribution_view<typename V::view_container_type>
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if a type is an instantiation of a view-based
///   distribution specification.
///
/// Specialization for the base case where the container of a view-based
/// distribution specification is a system_container instance, or other
/// views will be defined over other containers.
//////////////////////////////////////////////////////////////////////
template <typename V>
struct is_distribution_view<V, false>
  : public std::is_same<V, dist_view_impl::system_container>
{ };


template <typename DistributionView, typename PartitionInfoContainer>
struct view_based_partition;

template <typename DistributionView>
struct view_based_mapper;

//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the partition and mapper classes of a
///  container's distribution use a view-based distribution specification.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct is_view_based
  : public boost::mpl::false_
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the partition and mapper classes of a
///  container's distribution use a view-based distribution specification.
///
/// Specialization for the view-based partition class
//////////////////////////////////////////////////////////////////////
template <typename DistributionView, typename PartitionInfoContainer>
struct is_view_based<
         view_based_partition<DistributionView, PartitionInfoContainer>>
  : public boost::mpl::true_
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the partition and mapper classes of a
///  container's distribution use a view-based distribution specification.
///
/// Specialization for the view-based mapper class
//////////////////////////////////////////////////////////////////////
template <typename DistributionView>
struct is_view_based<view_based_mapper<DistributionView>>
  : public boost::mpl::true_
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the view is defined over a container that
///  is distributed using an arbitrary distribution.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct is_arbitrary_view_based
{
  template <typename View>
  bool operator()(View const*)
  { return false; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the view is defined over a container that
///  is distributed using an arbitrary distribution.
///
/// Specialization for the cases where the view_based_parition has been
/// provided with a container of @ref arbitrary_partition_info elements
/// specifying the arbitrary distribution.
//////////////////////////////////////////////////////////////////////
template <typename DistributionView, typename PartitionInfoContainer>
struct is_arbitrary_view_based<
         view_based_partition<DistributionView, PartitionInfoContainer>>
{
  template <typename View>
  bool operator()(View const*)
  { return true; }
};


//////////////////////////////////////////////////////////////////////
/// @brief Used to determine if the view is defined over a container that
///  is distributed using an arbitrary distribution.
///
/// Specialization for the cases where the view_based_parition is not given
/// a container of @ref arbitrary_partition_info elements, and the flag in
/// the @ref distribution_spec_view specifying the distribution is queried.
//////////////////////////////////////////////////////////////////////
template <typename DistributionView>
struct is_arbitrary_view_based<
         view_based_partition<DistributionView, int>>
{
  template <typename View>
  bool operator()(View const* view)
  { return this->operator()(view->container().get_distribution()); }

  template <typename Distribution>
  bool operator()(Distribution* dist)
  { return dist->partition().get_dist_view()->arbitrary(); }
};

template <typename Partition>
struct is_balanced_distribution
{
  bool operator()(Partition const&)
  { return false; }
};

template <typename DistributionView>
struct is_balanced_distribution<view_based_partition<DistributionView, int>>
{
  bool operator()(view_based_partition<DistributionView, int> const& part)
  {
    return part.get_dist_view()->distribution() == distribution_type::balanced;
  }
};
} // namespace stapl

#endif // STAPL_CONTAINERS_DISTRIBUTION_IS_DISTRIBUTION_VIEW_HPP
