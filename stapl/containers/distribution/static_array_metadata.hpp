/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DISTRIBUTION_STATIC_ARRAY_METADATA_HPP
#define STAPL_CONTAINERS_DISTRIBUTION_STATIC_ARRAY_METADATA_HPP

#include <stapl/views/metadata/container/container_wrapper.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Class for computing the metadata of @ref static_array container that
/// uses @ref container_manager_static that inherits from the base-container,
/// and thus, is the base-container itself. This type of container manager
/// has only one component per location.
/// @tparam Distribution Type of the Distribution.
/// @note this is different from the @ref static_container_extractor primarily
/// due to the metadata used. This class uses the
/// @ref metadata_container_wrapper
/// whereas the @ref static_metadata uses the metadata::flat_container.
//////////////////////////////////////////////////////////////////////
template<typename Distribution>
struct static_array_metadata
{
  typedef metadata::container_wrapper<Distribution>    md_cont_type;
  typedef typename md_cont_type::index_type            index_type;
  typedef std::pair<bool, md_cont_type*>               return_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Return the metadata of specified container.
  /// @see metadata_entry.
  /// @param cont A pointer to the container.
  ///
  /// Calls the operator on the distribution of the provided container.
  ///
  /// @return a pair indicating if the metadata container is static and
  ///   balance distributed and a pointer to the metadata container.
  //////////////////////////////////////////////////////////////////////
  template <typename Container>
  return_type operator()(Container* container)
  {
    return operator()(&(container->distribution()));
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Return the metadata of specified distribution.
  /// @see metadata_entry.
  /// @param dist A pointer to the distribution.
  ///
  /// @return a pair indicating if the metadata container is static and
  ///   balance distributed and a pointer to the metadata container.
  //////////////////////////////////////////////////////////////////////
  return_type operator()(Distribution* dist)
  {
    return std::make_pair(true, new md_cont_type(dist));
  }
};

} // namespace stapl

#endif // STAPL_CONTAINERS_DISTRIBUTION_STATIC_ARRAY_METADATA_HPP
