/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_LOCAL_PARTITION_INFO_FWD_HPP
#define STAPL_CONTAINERS_LOCAL_PARTITION_INFO_FWD_HPP

#include <stapl/utility/tuple.hpp>
#include <boost/icl/interval_set.hpp>

namespace stapl {

namespace cm_impl {

template <typename Partition, typename Mapper>
std::vector<tuple<
  boost::icl::interval_set<typename Partition::value_type::index_type>,
  typename Mapper::cid_type, location_type> >
get_partial_partition_info(Partition const& partition, Mapper const& mapper);

template <typename PartitionContainer>
std::vector<tuple<
  boost::icl::interval_set<unsigned long int>, unsigned long int, location_type
>>
get_partial_partition_info(PartitionContainer const* const part_cont);

template <typename PartInfo>
PartInfo get_local_partition_info(PartInfo const& partial_info);

} // namespace cm_impl

} // namespace stapl

#endif
