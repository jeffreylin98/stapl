/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_CONTAINERS_GRAPH_IS_STATIC_HPP
#define STAPL_CONTAINERS_GRAPH_IS_STATIC_HPP

#include <boost/mpl/bool.hpp>
#include <stapl/domains/iterator_domain.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Class for differentiating static and dynamic graphs based on
/// domain-type of the graph.
/// @tparam T type of the container.
/// @ingroup pgraphImpl
//////////////////////////////////////////////////////////////////////
template <typename T>
struct is_static
  : public boost::true_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Class for differentiating static and dynamic graphs based on
/// domain-type of the graph.
/// @tparam C type of the container.
/// @tparam D type of the domain.
/// @ingroup pgraphImpl
//////////////////////////////////////////////////////////////////////
template <typename C, typename D>
struct is_static<iterator_domain<C, D> >
  : public boost::false_type
{ };

} // namespace stapl

#endif /* STAPL_CONTAINERS_GRAPH_IS_STATIC_HPP */
