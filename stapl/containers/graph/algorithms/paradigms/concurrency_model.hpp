/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_CONCURRENCY_MODEL_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_CONCURRENCY_MODEL_HPP

#include <boost/mpl/has_xxx.hpp>

namespace stapl {

namespace sgl {

//////////////////////////////////////////////////////////////////////
/// @brief Tag to specify a strong concurrency model.
///
///        In a strong SGL concurrency model, a vertex operator and a neighbor
///        operator cannot be executed concurrently on the same vertex. This
///        model allows for shared state between the two operators to be
///        operated on with mutual exclusion.
//////////////////////////////////////////////////////////////////////
struct strong_concurrency {};

//////////////////////////////////////////////////////////////////////
/// @brief Tag to specify a weak concurrency model.
///
///        In a weak SGL concurrency model, a vertex operator and a neighbor
///        operator can be executed concurrently on the same vertex. This
///        model should only be used under the following conditions:
///          1. There is no shared state between the two operators
///          2. Visitations / graph methods only occur as the very last
///             statement of the vertex operator
//////////////////////////////////////////////////////////////////////
struct weak_concurrency {};

BOOST_MPL_HAS_XXX_TRAIT_DEF(concurrency_model)

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to extract the concurrency model of an SGL algorithm
///        based on its vertex operator.
//////////////////////////////////////////////////////////////////////
template<typename VertexOp, bool = has_concurrency_model<VertexOp>::type::value>
struct operator_concurrency_model
{
  using type = typename VertexOp::concurrency_model;
};

//////////////////////////////////////////////////////////////////////
/// @brief Default value for when there is no concurrency model specifed
//////////////////////////////////////////////////////////////////////
template<typename VertexOp>
struct operator_concurrency_model<VertexOp, false>
{
  using type = strong_concurrency;
};

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine whether an SGL algorithm has a strong
///        concurrency model based on its vertex operator.
//////////////////////////////////////////////////////////////////////
template <typename VertexOp>
struct has_strong_concurrency_model
  : std::is_same<typename operator_concurrency_model<VertexOp>::type,
                 strong_concurrency>
{};

} // namespace sgl

} // namespace stapl

#endif
