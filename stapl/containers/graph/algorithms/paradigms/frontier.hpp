/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_FRONTIER_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_FRONTIER_HPP

#include <stapl/runtime.hpp>
#include <stapl/containers/graph/algorithms/superstep_occupancy.hpp>
#include <stapl/containers/graph/algorithms/paradigms/ordering.hpp>
#include <stapl/containers/sequential/bitset/bitset.hpp>
#include <stapl/views/type_traits/is_contiguous_domain.hpp>

#include <vector>

namespace stapl {

namespace sgl {

/// The ratio of active to inactive vertices at which a frontier
/// will be considered dense
constexpr double dense_frontier_point = 0.75;

//////////////////////////////////////////////////////////////////////
/// @brief Set of active vertices during a traversal
/// @tparam Set The GID set type that will store the active vertices' GIDs
//////////////////////////////////////////////////////////////////////
template<typename Set>
class sparse_frontier
  : public p_object
{
  /// Current frontier that is being processed
  Set m_current;
  /// Frontier for the next superstep
  Set m_next;

public:
  using set_type = Set;
  using descriptor_type = typename set_type::gid_type;
  using iterator = typename set_type::iterator;

  sparse_frontier(Set current, Set next)
    : m_current(std::move(current)), m_next(std::move(next))
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Mark a vertex as active in the next superstep
  /// @param The GID of the vertex to add
  //////////////////////////////////////////////////////////////////////
  __attribute__((always_inline)) inline void add(descriptor_type const& v)
  {
    m_next.add(v);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Iterator to the first vertex in the frontier
  //////////////////////////////////////////////////////////////////////
  iterator begin()
  {
    return m_current.begin();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Iterator to one past the last vertex in the frontier
  //////////////////////////////////////////////////////////////////////
  iterator end()
  {
    return m_current.end();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Advance the frontier by setting the next superstep's frontier
  /// to the current frontier
  //////////////////////////////////////////////////////////////////////
  void advance(void)
  {
    m_current.swap(m_next);
    m_next.clear();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Storage type of the implementation of a frontier
//////////////////////////////////////////////////////////////////////
enum class frontier_type
{
  vector, bitmap, implied, unordered_set
};


namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Implementation for making an empty frontier
///
/// This is the default implementation using standard vector storage
/// for the frontier
//////////////////////////////////////////////////////////////////////
template <typename Container,
          frontier_type FrontierType,
          bool HasContiguousDomain = is_contiguous_domain<
            typename Container::distribution_type::container_manager_type::
              base_container_type::domain_type>::value>
struct make_empty_frontier_impl
{
  using type = sgl::sparse_frontier<typename Container::distribution_type::
                                      container_manager_type::gid_set_type>;

  static type create(Container& cont)
  {
    return { cont.distribution().container_manager().make_gid_set(),
             cont.distribution().container_manager().make_gid_set() };
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Implementation for making an empty frontier
///
/// This is the specialization using no-storage, where the frontier is
/// all of the local vertices.
//////////////////////////////////////////////////////////////////////
template <typename Container, bool HasContiguousDomain>
struct make_empty_frontier_impl<Container,
                                frontier_type::implied,
                                HasContiguousDomain>
{
  using type
    = sgl::sparse_frontier<typename Container::distribution_type::
                             container_manager_type::implied_gid_set_type>;

  static type create(Container& cont)
  {
    return { cont.distribution().container_manager().make_implied_gid_set(),
             cont.distribution().container_manager().make_implied_gid_set() };
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Implementation for making an empty frontier
///
/// This is the specialization using a unique set for the GIDs
//////////////////////////////////////////////////////////////////////
template <typename Container, bool HasContiguousDomain>
struct make_empty_frontier_impl<Container,
                                frontier_type::unordered_set,
                                HasContiguousDomain>
{
  using type
    = sgl::sparse_frontier<typename Container::distribution_type::
                             container_manager_type::unique_gid_set_type>;

  static type create(Container& cont)
  {
    return { cont.distribution().container_manager().make_unique_gid_set(),
             cont.distribution().container_manager().make_unique_gid_set() };
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Implementation for making an empty frontier
///
/// This specialization for bitmap storage is only applicable to containers
/// with a contiguous domain
//////////////////////////////////////////////////////////////////////
template <typename Container>
struct make_empty_frontier_impl<Container, frontier_type::bitmap, true>
{
  using type
    = sgl::sparse_frontier<typename Container::distribution_type::
                             container_manager_type::bitmap_gid_set_type>;

  static type create(Container& cont)
  {
    return { cont.distribution().container_manager().make_bitmap_gid_set(),
             cont.distribution().container_manager().make_bitmap_gid_set() };
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Make an empty frontier for a given container
/// @tparam FrontierType The storage type of the frontier
/// @param cont The graph container
//////////////////////////////////////////////////////////////////////
template<frontier_type FrontierType, typename Container>
typename detail::make_empty_frontier_impl<Container, FrontierType>::type
make_empty_frontier(Container& cont)
{
  return detail::make_empty_frontier_impl<Container, FrontierType>::create(
    cont);
}

//////////////////////////////////////////////////////////////////////
/// @brief Compute the GID set storage type that is appropriate for
/// a graph algorithm, based on the algorithm's ordering requirements
/// and superstep occupancy information.
/// @tparam FrontierType The requested storage type of the frontier
/// @tparam OrderingType The graph algorithm's ordering type
/// @tparam SuperstepOccupancy The graph algorithm's superstep occupancy
//////////////////////////////////////////////////////////////////////
template <frontier_type FrontierType,
          ordering_type OrderingType,
          typename SuperstepOccupancy>
struct compute_gid_set_type
{
  static constexpr frontier_type value = FrontierType;
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for when every superstep processes every vertex.
/// In such a case, the implied GID set storage is the best choice.
//////////////////////////////////////////////////////////////////////
template <frontier_type FrontierType, ordering_type OrderingType>
struct compute_gid_set_type<FrontierType,
                            OrderingType,
                            always_active_superstep_occupancy_type>
{
  static constexpr frontier_type value = frontier_type::implied;
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for when the algorithm requires that the
/// frontier be a unique set and the requested GID set is a vector store.
/// In such a case, it is necessary to use an unordered_set instead
/// of a vector.
//////////////////////////////////////////////////////////////////////
template <typename SuperstepOccupancy>
struct compute_gid_set_type<frontier_type::vector,
                            ordering_type::interleaved,
                            SuperstepOccupancy>
{
  static constexpr frontier_type value = frontier_type::unordered_set;
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function to apply the user's initialization operator
/// to every vertex in the graph and to add active vertices to the
/// frontier.
/// @tparam F The user's initialization operator
/// @tparam Frontier The frontier type
/// @tparam AddToFrontier Whether or not active vertices should be
/// actually added to the frontier
//////////////////////////////////////////////////////////////////////
template<typename F, typename Frontier, bool AddToFrontier>
struct frontier_function_apply
{
  F m_frontier_function;
  Frontier* m_frontier;

  frontier_function_apply(F frontier_function, Frontier& frontier)
    : m_frontier_function(std::move(frontier_function)), m_frontier(&frontier)
  { }

  template<typename Vertex>
  void operator()(Vertex&& v) const
  {
    const bool active = m_frontier_function(std::forward<Vertex>(v));
    if (AddToFrontier && active)
      m_frontier->add(v.descriptor());
  }

  void define_type(stapl::typer& t)
  {
    t.member(m_frontier_function);
    t.member(m_frontier);
  }

};

//////////////////////////////////////////////////////////////////////
/// @brief Initialize the frontier by applying the user's initialization
/// operator to every vertex.
/// @tparam OccupancyType The superstep occupancy of the graph algorithm
/// @param frontier The frontier to initialize
/// @param frontier_function The user's initialization operator
/// @param g The graph
//////////////////////////////////////////////////////////////////////
template<typename OccupancyType, typename Frontier, typename F, typename Graph>
void
initialize_frontier(Frontier& frontier, F&& frontier_function, Graph&& g)
{
  // If we know that the first superstep is going to process them all, we
  // don't need to bother populating the frontier
  constexpr bool add_to_frontier
    = !first_superstep_has_all_vertices<OccupancyType>::value;

  map_func(
    frontier_function_apply<typename std::decay<F>::type,
                            Frontier,
                            add_to_frontier>{
      std::forward<F>(frontier_function), frontier },
    std::forward<Graph>(g));

  if (add_to_frontier)
    frontier.advance();
}

} // namespace sgl

} // namespace stapl

#endif
