/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PROPERTIES_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PROPERTIES_HPP

#include <stapl/containers/graph/graph.hpp>
#include <boost/serialization/deque.hpp>
#include <stapl/views/proxy_macros.hpp>

namespace stapl {

namespace properties {

//////////////////////////////////////////////////////////////////////
/// @brief Vertex property for use with @ref breadth_first_search().
/// @ingroup pgraphAlgoProps
//////////////////////////////////////////////////////////////////////
class bfs_property
{
private:
  typedef size_t VD;
  typedef size_t Level;

  VD    m_vd;
  Level m_level;

public:
  typedef VD    parent_type;
  typedef Level level_type;

  bfs_property(void)
    : m_vd(std::numeric_limits<VD>::max()),
      m_level(std::numeric_limits<Level>::max())
  { }

  inline VD parent(void) const
  { return m_vd; }

  inline void parent(VD const& vd)
  { m_vd = vd; }

  inline Level level(void) const
  { return m_level; }

  inline void level(Level const& c)
  { m_level = c; }

  void define_type(stapl::typer& t)
  {
    t.member(m_vd);
    t.member(m_level);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Vertex property for use with @ref page_rank().
/// @ingroup pgraphAlgoProps
//////////////////////////////////////////////////////////////////////
template<typename Rank>
class page_rank_property
{
public:
  using value_type = Rank;

private:
  value_type m_color;
  value_type m_color2;

public:
  page_rank_property(void)
    : m_color(0.0), m_color2(0.0)
  { }

  value_type rank(void) const
  { return m_color;  }

  void rank(value_type val)
  { m_color = val;  }

  value_type new_rank(void) const
  { return m_color2; }

  void new_rank(value_type val)
  { m_color2 = val; }

  void define_type(stapl::typer& t)
  {
    t.member(m_color);
    t.member(m_color2);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Vertex property for use with @ref connected_components().
/// @ingroup pgraphAlgoProps
//////////////////////////////////////////////////////////////////////
class cc_property
{
private:
  size_t m_cc;
  bool m_active;

public:
  cc_property(void)
    : m_cc(std::numeric_limits<size_t>::max()), m_active(true)
  { }

  cc_property(size_t c)
    : m_cc(c), m_active(false)
  { }

  void cc(size_t c)
  { m_cc = c; }

  size_t cc(void) const
  { return m_cc; }

  void active(bool a)
  { m_active = a; }

  bool active(void) const
  { return m_active; }

  void define_type(stapl::typer& t)
  {
    t.member(m_cc);
    t.member(m_active);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Vertex property for use with @ref betweenness_centrality().
/// Set of properties for each vertex, distance, delta, sigma,
/// list of parent vertices, and a marker for BFS visitation.
/// @ingroup pgraphAlgoProps
//////////////////////////////////////////////////////////////////////
struct bc_property
{
  typedef size_t                    vd_type;
  typedef size_t                    dist_type;
  typedef double                    sigma_type;
  typedef double                    delta_type;
  typedef std::vector<vd_type>      bfs_dag_type;
  typedef std::vector<vd_type>      level_type;

  typedef std::vector<dist_type>    distances_t;
  typedef std::vector<sigma_type>   sigmas_t;
  typedef std::vector<delta_type>   deltas_t;
  typedef std::vector<bfs_dag_type> bfs_dags_t;
  typedef std::deque<vd_type>       active_t;
  typedef std::vector<level_type>   levels_t;

  delta_type  m_BC;
  distances_t m_distances;
  sigmas_t    m_sigmas;
  deltas_t    m_deltas;
  bfs_dags_t  m_bfs_dags;
  active_t    m_active1;
  active_t    m_active2;
  bool        m_active_flip;
  levels_t    m_levels;

  bc_property(void)
    : m_BC(0.0), m_active_flip(true)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Initializes member properties and allocates space for
  /// a given number of sources.
  //////////////////////////////////////////////////////////////////////
  void initialize(size_t num_sources)
  {
    m_BC = 0.0;
    m_active_flip = true;

    m_distances.clear();
    m_sigmas.clear();
    m_deltas.clear();
    m_bfs_dags.clear();
    m_active1.clear();
    m_active2.clear();
    m_levels.clear();

    m_distances.resize(num_sources, 0);
    m_sigmas.resize(num_sources, 0);
    m_deltas.resize(num_sources, 0);
    m_bfs_dags.resize(num_sources);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Resets member properties before calculating next set of
  /// sources.
  //////////////////////////////////////////////////////////////////////
  void reset_sources(void)
  {
    m_active_flip = true;
    m_active1.clear();
    m_active2.clear();
    m_levels.clear();
    std::fill(m_distances.begin(), m_distances.end(), 0);
    std::fill(m_sigmas.begin(), m_sigmas.end(), 0.0);
    std::fill(m_deltas.begin(), m_deltas.end(), 0.0);
    for (size_t i=0; i<m_bfs_dags.size(); ++i)
      m_bfs_dags[i].clear();
  }

  inline delta_type BC(void) const
  { return m_BC; }

  inline void BC(delta_type bc)
  { m_BC = bc; }

  inline dist_type distance(vd_type source_id) const
  { return m_distances[source_id]; }

  inline dist_type max_distance() const
  {
    auto max_it = std::max_element(m_distances.begin(), m_distances.end());
    stapl_assert(max_it != m_distances.end(), "Empty distance map");
    return *max_it;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Sets the distance of vertex from source to the value dist.
  /// Also adds the source id to list of sources at level=dist.
  //////////////////////////////////////////////////////////////////////
  inline void distance(vd_type source_id, dist_type dist)
  {
    m_distances[source_id] = dist;
    if (dist >= m_levels.size())
      m_levels.resize(dist+1);
    m_levels[dist].push_back(source_id);
  }

  inline delta_type delta(vd_type source_id) const
  { return m_deltas[source_id]; }

  inline void delta(vd_type source_id, delta_type delta)
  { m_deltas[source_id] = delta; }

  inline sigma_type sigma(vd_type source_id) const
  { return m_sigmas[source_id]; }

  inline void sigma(vd_type source_id, sigma_type sigma)
  { m_sigmas[source_id] = sigma; }

  inline const bfs_dag_type bfs_dag(vd_type source_id) const
  { return m_bfs_dags[source_id]; }

  //////////////////////////////////////////////////////////////////////
  /// @brief Appends vertex to bfs dag if it is not already in the bfs dag
  /// and increments sigma value.
  /// @param source_id Source id of currently active source
  /// @param p_id Numeric id of parent vertex
  /// @param sig Sigma value of parent vertex
  //////////////////////////////////////////////////////////////////////
  inline void bfs_dag(vd_type source_id, vd_type p_id, sigma_type sig)
  {
    bfs_dag_type::const_iterator it =
        std::find(m_bfs_dags[source_id].begin(),
                  m_bfs_dags[source_id].end(), p_id);
    if (it == m_bfs_dags[source_id].end()) {
      m_bfs_dags[source_id].push_back(p_id);
      m_sigmas[source_id] += sig;
    }
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns a list of sources that are on the same level (i.e.
  /// distance away from this vertex). Used in calculating delta, where
  /// delta is recursively defined starting from last level.
  /// @param level Level is equal to distance from source to this
  /// vertex.
  /// @see dependency_wf
  //////////////////////////////////////////////////////////////////////
  inline const level_type level_sources(dist_type level) const
  {
    if (level < m_levels.size())
      return m_levels[level];
    else
      return level_type();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Gets the next active source id and removes it from the list.
  //////////////////////////////////////////////////////////////////////
  inline vd_type next_active_traversal(void)
  {
    if (m_active_flip) {
      vd_type vd = m_active1.front();
      m_active1.pop_front();
      return vd;
    } else {
      vd_type vd = m_active2.front();
      m_active2.pop_front();
      return vd;
    }
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Checks if there are any active sources left to process.
  //////////////////////////////////////////////////////////////////////
  inline bool is_active(void) const
  {
    if (m_active_flip)
      return !m_active1.empty();
    else
      return !m_active2.empty();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Adds a source id to the list of active sources.
  /// @see mssp_wf
  //////////////////////////////////////////////////////////////////////
  inline void set_active(vd_type source_id)
  {
    if (m_active_flip)
      m_active1.push_back(source_id);
    else
      m_active2.push_back(source_id);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Adds a source id to the list of pending sources to be
  /// worked on the next level of mssp.
  /// @see mssp_wf
  //////////////////////////////////////////////////////////////////////
  inline void set_pending(vd_type source_id)
  {
    if (m_active_flip)
      m_active2.push_back(source_id);
    else
      m_active1.push_back(source_id);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Clears the finished list of active sources worked on and
  /// sets the pending sources to be worked on next level.
  /// @see mssp_lsync
  /// @see post_execute
  //////////////////////////////////////////////////////////////////////
  inline void push_pending(void)
  { m_active_flip = !m_active_flip; }

  void define_type(typer& t)
  {
    t.member(m_BC);
    t.member(m_distances);
    t.member(m_sigmas);
    t.member(m_deltas);
    t.member(m_bfs_dags);
    t.member(m_active1);
    t.member(m_active2);
    t.member(m_active_flip);
    t.member(m_levels);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Vertex property for use with @ref sssp().
/// @ingroup pgraphAlgoProps
//////////////////////////////////////////////////////////////////////
class sssp_property
{
private:
  typedef size_t VD;
  VD     m_vd;
  double m_distance;
public:
  typedef VD     parent_type;
  typedef double dist_type;

  inline VD parent(void) const
  { return m_vd; }

  inline dist_type distance(void) const
  { return m_distance; }

  inline void parent(VD const& vd)
  { m_vd = vd; }

  inline void distance(dist_type const& c)
  { m_distance = c; }

  void define_type(stapl::typer& t)
  {
    t.member(m_vd);
    t.member(m_distance);
  }
};

} //namespace properties


template <class Accessor>
class proxy<properties::bfs_property, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef properties::bfs_property target_t;

public:
  typedef size_t vd_type;
  typedef size_t VD;
  typedef size_t Level;
  typedef size_t value_type;

  explicit proxy(Accessor const& acc)
    : Accessor(acc)
  { }

  operator target_t(void) const
  { return Accessor::read(); }

  proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this;}

  VD parent(void) const
  {
    VD (target_t::*pmf)(void) const = &target_t::parent;
    return Accessor::const_invoke(pmf);
  }

  void parent(VD const& vd)
  {
    void (target_t::*pmf)(VD const&) = &target_t::parent;
    Accessor::invoke(pmf, vd);
  }

  Level level(void) const
  {
    Level (target_t::*pmf)(void) const = &target_t::level;
    return Accessor::const_invoke(pmf);
  }

  void level(Level const& c)
  {
    void (target_t::*pmf)(Level const&) = &target_t::level;
    Accessor::invoke(pmf, c);
  }
}; //class bfs_property proxy

template<typename Rank, typename Accessor>
class proxy<properties::page_rank_property<Rank>, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef properties::page_rank_property<Rank> target_t;
  typedef typename target_t::value_type value_type;

public:
  explicit proxy(Accessor const& acc)
    : Accessor(acc)
  { }

  operator target_t(void) const
  { return Accessor::read(); }

  proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this; }

  value_type rank(void) const
  { return Accessor::const_invoke(&target_t::rank); }

  void rank(value_type val)
  { Accessor::invoke(&target_t::rank, val); }

  value_type new_rank(void) const
  { return Accessor::const_invoke(&target_t::new_rank); }

  void new_rank(value_type val)
  { Accessor::invoke(&target_t::new_rank, val); }

}; //class page_rank_property proxy

template <typename Accessor>
class proxy<properties::cc_property, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef properties::cc_property target_t;

public:
  explicit proxy(Accessor const& acc)
    : Accessor(acc) { }

  operator target_t(void) const
  { return Accessor::read(); }

  proxy const& operator=(proxy const& rhs) {
    Accessor::write(rhs);
    return *this;
  }

  proxy const& operator=(target_t const& rhs) {
    Accessor::write(rhs);
    return *this;
  }

  void cc(size_t c)
  { Accessor::invoke(&target_t::cc, c); }

  size_t cc(void) const
  { return Accessor::const_invoke(&target_t::cc); }

  void active(bool a)
  { Accessor::invoke(&target_t::active, a); }

  bool active(void) const
  { return Accessor::const_invoke(&target_t::active); }
}; //class cc_property proxy

template <typename Accessor>
class proxy<properties::bc_property, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef properties::bc_property target_t;
public:
  typedef typename target_t::vd_type      vd_type;
  typedef typename target_t::dist_type    dist_type;
  typedef typename target_t::sigma_type   sigma_type;
  typedef typename target_t::delta_type   delta_type;
  typedef typename target_t::bfs_dag_type bfs_dag_type;
  typedef typename target_t::level_type   level_type;

  typedef typename target_t::distances_t  distances_t;
  typedef typename target_t::sigmas_t     sigmas_t;
  typedef typename target_t::distances_t  deltas_t;
  typedef typename target_t::bfs_dags_t   bfs_dags_t;
  typedef typename target_t::active_t     active_t;

  inline explicit proxy(Accessor const& acc)
    : Accessor(acc)
  { }

  inline operator target_t(void) const
  { return Accessor::read(); }

  inline proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  inline proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this; }

  inline void initialize(size_t num_sources)
  { Accessor::invoke(&target_t::initialize, num_sources); }

  inline void reset_sources(void)
  { Accessor::invoke(&target_t::reset_sources); }

  inline delta_type BC(void) const
  { return Accessor::const_invoke(&target_t::BC); }

  inline delta_type max_distance(void) const
  { return Accessor::const_invoke(&target_t::max_distance); }

  inline void BC(delta_type bc)
  { Accessor::invoke(&target_t::BC, bc); }

  inline dist_type distance(vd_type source_id) const
  { return Accessor::const_invoke(&target_t::distance, source_id); }

  inline void distance(vd_type source_id, dist_type dist)
  { Accessor::invoke(&target_t::distance, source_id, dist); }

  inline delta_type delta(vd_type source_id) const
  { return Accessor::const_invoke(&target_t::delta, source_id); }

  inline void delta(vd_type source_id, delta_type delta)
  { Accessor::invoke(&target_t::delta, source_id, delta); }

  inline sigma_type sigma(vd_type source_id) const
  { return Accessor::const_invoke(&target_t::sigma, source_id); }

  inline void sigma(vd_type source_id, sigma_type value)
  { Accessor::invoke(&target_t::sigma, source_id, value); }

  inline const bfs_dag_type bfs_dag(vd_type source_id) const
  { return Accessor::const_invoke(&target_t::bfs_dag, source_id); }

  inline void bfs_dag(vd_type source_id, vd_type p_id, sigma_type sig)
  { Accessor::invoke(&target_t::bfs_dag, source_id, p_id, sig); }

  inline const level_type level_sources(dist_type level) const
  { return Accessor::const_invoke(&target_t::level_sources, level); }

  inline vd_type next_active_traversal(void)
  { return Accessor::invoke(&target_t::next_active_traversal); }

  inline bool is_active(void) const
  { return Accessor::const_invoke(&target_t::is_active); }

  inline void set_active(vd_type source_id)
  { Accessor::invoke(&target_t::set_pending, source_id); }

  inline void set_pending(vd_type source_id)
  { Accessor::invoke(&target_t::set_pending, source_id); }

  inline void push_pending(void)
  { Accessor::invoke(&target_t::push_pending); }
}; //class bc_property proxy

template <class Accessor>
class proxy<properties::sssp_property, Accessor>
  : public Accessor
{
private:
  friend class proxy_core_access;
  typedef properties::sssp_property target_t;

public:
  typedef target_t::parent_type   parent_type;
  typedef typename target_t::dist_type dist_type;

  inline explicit proxy(Accessor const& acc)
    : Accessor(acc)
  { }

  inline operator target_t(void) const
  { return Accessor::read(); }

  inline proxy const& operator=(proxy const& rhs)
  { Accessor::write(rhs); return *this; }

  inline proxy const& operator=(target_t const& rhs)
  { Accessor::write(rhs); return *this;}

  inline parent_type parent(void) const
  { return Accessor::const_invoke(&target_t::parent); }

  inline dist_type distance(void) const
  { return Accessor::const_invoke(&target_t::distance); }

  inline void  parent(parent_type const& vd)
  { Accessor::invoke(&target_t::parent, vd); }

  inline void  distance(dist_type const& c)
  { Accessor::invoke(&target_t::distance, c); }

}; //class sssp_property proxy

} //namespace stapl

#endif
