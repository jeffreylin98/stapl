/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_VECTOR_BASE_CONTAINER_TRAITS_HPP
#define STAPL_CONTAINERS_VECTOR_BASE_CONTAINER_TRAITS_HPP

#include <vector>

#include <stapl/domains/indexed.hpp>
#include <stapl/containers/type_traits/define_value_type.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Default traits for the vector base container. These traits
///        can be changed to customize base containers.
/// @ingroup pvectorTraits
/// @tparam T Type of the stored elements in the base container.
/// @ingroup pvectorTraits
////////////////////////////////////////////////////////////////////////
template <typename T>
struct vector_base_container_traits
{
  typedef typename define_value_type<T>::type   stored_type;
  typedef std::vector<stored_type>              container_type;
  typedef container_type                        container_constructor;
  typedef T                                     value_type;
  typedef indexed_domain<size_t>                domain_type;
};

} // namespace stapl

#endif // STAPL_CONTAINERS_VECTOR_BASE_CONTAINER_TRAITS_HPP
