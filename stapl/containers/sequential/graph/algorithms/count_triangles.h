/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_SEQUENTIAL_COUNT_TRIANGLES_HPP
#define STAPL_CONTAINERS_SEQUENTIAL_COUNT_TRIANGLES_HPP

#include "graph_algo_util.h"

namespace stapl {
namespace sequential{

////////////////////////////////////////////////////////////////////////
/// @brief Counts the number of 'triangles' in a graph that contain the given
/// vertex @p v.
/// @param g the UNDIRECTED graph
/// @param v the vertex whose triangles are to be counted
/// @return the number of triangles
/// @note This algorithm is guaranteed correct only for undirected graphs
/// that contain no self-loops or parallel edges.
/// @note The complexity for the algorithm is O(|E|).
/// @ingroup seqGraphAlgo
/////////////////////////////////////////////////////////////////////////
template<typename Graph>
size_t count_local_triangles(Graph& g, typename Graph::vertex_descriptor v)
{
  typedef typename Graph::vertex_iterator   vi_type;
  typedef typename Graph::adj_edge_iterator aei_type;

  map_property_map<Graph, bool> nmap;

  size_t triangle_count = 0;
  vi_type it = g.find_vertex(v);
  aei_type aei, aei_end = (*it).end();
  aei_type naei, naei_end; //neighbor's neighbors
  //we mark v's neighbors as we go along; this should turn out correct
  for (aei = (*it).begin(); aei != aei_end; ++aei) {
    nmap.put((*aei).target(), true); //target is a neighbor

    //now find the triangles
    //NOTE: reassigning it here; shouldn't need v anymore
    it = g.find_vertex((*aei).target());
    naei_end = (*it).end();
    for (naei = (*it).begin(); naei != naei_end; ++naei) {
      if (nmap.get((*naei).target()))
        ++triangle_count;
    }
  }

  //shouldn't need to divide by 2; I think each triangle is seen only once.
  //return triangle_count/2;
  return triangle_count;
}

}//end namespace sequential
}//end namespace stapl

#endif
