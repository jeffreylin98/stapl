/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_VIEWS_BALANCE_VIEW_HPP
#define STAPL_VIEWS_BALANCE_VIEW_HPP

#include <stapl/views/segmented_view.hpp>
#include <stapl/containers/partitions/balanced.hpp>

#include <stapl/views/common_view.hpp>

namespace stapl {

namespace view_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Functor to construct a partitioned view using a balanced
///        partitioner
/// @ingroup balance_view
//////////////////////////////////////////////////////////////////////
template<typename View>
class part_balance_view :
  public common_view
{
  typedef typename View::domain_type                   domain_type;
  typedef balanced_partition<domain_type>              pbal_type;

public:
  typedef segmented_view<View,pbal_type>             view_type;

  //////////////////////////////////////////////////////////////////////
  /// @see stapl::balance_view
  //////////////////////////////////////////////////////////////////////
  view_type operator()(View& v, size_t n) {
    return view_type(v,pbal_type(domain_type(v.domain().first(),
                                             v.domain().last(),
                                             v.domain()), n) );
  }

  //////////////////////////////////////////////////////////////////////
  /// @see stapl::balance_view
  //////////////////////////////////////////////////////////////////////
  view_type operator()(View const& v, size_t n) {
    return view_type(v,pbal_type(domain_type(v.domain().first(),
                                             v.domain().last(),
                                             v.domain()), n) );
  }

  //////////////////////////////////////////////////////////////////////
  /// @internal
  //////////////////////////////////////////////////////////////////////
  size_t version(void) const
  {
    return 0;
  }

  //////////////////////////////////////////////////////////////////////
  /// @internal
  //////////////////////////////////////////////////////////////////////
  bool validate()
  {
    return true;
  }
};

} // namespace view_impl

namespace result_of {

//////////////////////////////////////////////////////////////////////
/// @brief Defines balance_view type over View.
///
/// @tparam View to partition
//////////////////////////////////////////////////////////////////////
template<typename View>
struct balance_view
{
  typedef typename view_impl::part_balance_view<View>::view_type  type;
};

} // result_of namespace


//////////////////////////////////////////////////////////////////////
/// @brief Helper function to construct a balance_view
///
/// @param view to partition
/// @param n number of partitions
/// @return a balanced partitioned view
//////////////////////////////////////////////////////////////////////
template<typename View>
typename result_of::balance_view<View>::type
balance_view(const View& view, size_t n)
{
  return view_impl::part_balance_view<View>()(view,n);
}


} // stapl namespace

#endif /* STAPL_VIEWS_BALANCE_VIEW_HPP */
