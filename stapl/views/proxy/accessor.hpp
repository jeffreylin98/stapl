/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_PROXY_ACCESSOR_HPP
#define STAPL_VIEWS_PROXY_ACCESSOR_HPP

#include <boost/utility/result_of.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Helper class to access the private methods of the given
///        accessor.
///
/// The given accessor should have declared as a friend this class.
//////////////////////////////////////////////////////////////////////
class accessor_core_access
{
public:
  accessor_core_access(void) = delete;

  template<typename A, typename F>
  static void
  apply_set(A const& a, F const& f)
  {
    a.apply_set(f);
  }

  template<typename A, typename F>
  static
  typename boost::result_of<F(typename A::value_type)>::type
  apply_get(A const& a, F const& f)
  {
    return a.apply_get(f);
  }

  template<typename A, typename T>
  static void
  write(A& a, T const& val)
  {
    a.write(val);
  }

  template<typename A>
  static bool is_local(A const& a)
  {
    return a.is_local();
  }
}; // class accessor_core_access

} // namespace stapl

#endif // STAPL_VIEWS_PROXY_ACCESSOR_HPP
