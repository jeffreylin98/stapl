/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_VIEWS_TYPE_TRAITS_IS_DOMAIN_CONTIGUOUS_HPP
#define STAPL_VIEWS_TYPE_TRAITS_IS_DOMAIN_CONTIGUOUS_HPP

#include <stapl/domains/indexed.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute whether or not a domain represents
///        a contiguous chunk of GIDs
/// @tparam Domain The domain to query
//////////////////////////////////////////////////////////////////////
template<typename Domain>
struct is_contiguous_domain
  : std::false_type
{};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for indexed_domain
//////////////////////////////////////////////////////////////////////
template<typename T, int N, typename ...OptionalTraversal>
struct is_contiguous_domain<indexed_domain<T, N, OptionalTraversal...>>
  : std::true_type
{};

} // stapl namespace

#endif
