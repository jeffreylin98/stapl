/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_RETYPE_HPP
#define STAPL_VIEWS_RETYPE_HPP

#include <boost/static_assert.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Helper class to change the derived type of the given
///        @tparam View type to the given @tparam Child type.
//////////////////////////////////////////////////////////////////////
template<typename View, typename Child>
struct inherit_retype
{
  BOOST_STATIC_ASSERT(sizeof(View) == 0);
};


template<template<typename, typename...> class View, typename C, typename Child>
struct inherit_retype<View<C>, Child>
{
  typedef View<
    C,
    typename view_traits<View<C>>::domain_type,
    typename view_traits<View<C>>::map_function,
    Child
  > type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization to change the derived type of the given view.
//////////////////////////////////////////////////////////////////////
template<template<typename,typename,typename,typename> class VT,
         typename C, typename D, typename F, typename Derived, typename Child>
struct inherit_retype<VT<C,D,F,Derived>, Child>
{
  typedef VT<C,D,F,Child> type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper class to change the mapping function type of the
///        given @tparam View to the specified new mapping function type
///        @tparam NF. Base case.
//////////////////////////////////////////////////////////////////////
template<typename View, typename NF>
struct mapfunc_retype
{
  BOOST_STATIC_ASSERT(sizeof(View) == 0);
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for @ref mapfunc_retype to change the
///        mapping function from the given @tparam View type to the
///        specified new mapping function type @tparam NF.
//////////////////////////////////////////////////////////////////////
template<template<typename,typename,typename,typename> class VT,
         typename C, typename D, typename F, typename Derived, typename NF>
struct mapfunc_retype<VT<C,D,F,Derived>, NF>
{
  typedef VT<C,D,NF,Derived> type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper class to change the domain type of the given @tparam View
///        to the specified new domain type @tparam NewDom. Base case.
//////////////////////////////////////////////////////////////////////
template<typename View, typename NewDom>
struct domain_retype
{
  BOOST_STATIC_ASSERT(sizeof(View) == 0);
};

//////////////////////////////////////////////////////////////////////
/// @brief Specialization for @ref domain_retype to change the domain
///        from the given @tparam View type to the specified new
///        domain type @tparam NF.
//////////////////////////////////////////////////////////////////////
template<template<typename, typename, typename, typename> class V,
         typename C, typename D, typename M, typename De, typename NewDom>
struct domain_retype<V<C, D, M, De>, NewDom>
{
  typedef V<C, NewDom, M, De> type;
};

} //namespace stapl

#endif // STAPL_VIEWS_RETYPE_HPP
