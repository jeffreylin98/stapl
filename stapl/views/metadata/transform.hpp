/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_TRANSFORM_HPP
#define STAPL_VIEWS_METADATA_TRANSFORM_HPP

#include <stapl/views/metadata/coarsen_utility.hpp>

namespace stapl {

namespace metadata {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to construct a partitioned_mix_view from the
///        given view and metadata container.
//////////////////////////////////////////////////////////////////////
struct transform_view_from_metadata
{
  template <typename Signature>
  struct result;

  template <typename View, typename MDCont>
  struct result<transform_view_from_metadata(View, MDCont)>
  {
    using wrapper_t =
      metadata::view_wrapper<
        typename MDCont::value_type,
        typename MDCont::domain_type,
        metadata_traits<MDCont>::is_isomorphic::value
      >;

    using type = partitioned_mix_view<View, wrapper_t>;
  };

  template<typename View, typename MDCont>
  typename result<transform_view_from_metadata(View, MDCont)>::type
  operator()(View& view, MDCont& md_cont) const
  {
    typedef typename result<
      transform_view_from_metadata(View, MDCont)
    >::type                                                    return_t;
    typedef typename return_t::view_container_type             container_t;
    typedef metadata::view_wrapper<
      typename MDCont::value_type,
      typename MDCont::domain_type,
      metadata_traits<MDCont>::is_isomorphic::value
    > wrapper_t;


    md_cont.update();

    return return_t(new container_t(view, wrapper_t(new MDCont(md_cont))));
  }
}; // struct transform_view_from_metadata

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Transform fine-grained views into coarse-grained views based
///        on a set of metadata containers.
///
/// @param views A tuple of fine-grained views
/// @param md_conts A tuple of metadata corresponding metadata containers
/// @return A tuple of coarse-grained views.
//////////////////////////////////////////////////////////////////////
template<typename Views, typename MDConts>
auto transform(Views&& views, MDConts&& md_conts)
 -> decltype(
     vs_map(detail::transform_view_from_metadata(), views, md_conts)
   )
{
  return vs_map(detail::transform_view_from_metadata(),
    //std::forward<Views>(views), std::forward<MDConts>(md_conts)
    views, md_conts
  );
}

} // namespace metadata

} // namespace stapl

#endif
