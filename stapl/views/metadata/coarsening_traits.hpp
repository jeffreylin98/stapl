/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

// Coarsen partition based on locality information

#ifndef STAPL_VIEWS_METADATA_COARSENING_TRAITS_HPP
#define STAPL_VIEWS_METADATA_COARSENING_TRAITS_HPP

#include <stapl/views/metadata/projection/segmented.hpp>
#include <stapl/views/metadata/projection/container.hpp>
#include <stapl/views/metadata/projection/implicit.hpp>
#include <stapl/views/metadata/projection/invertible.hpp>

#include <stapl/views/type_traits/is_segmented_view.hpp>
#include <stapl/views/type_traits/is_invertible_view.hpp>
#include <stapl/containers/type_traits/is_container.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute the type of the metadata projection
///        function object.
//////////////////////////////////////////////////////////////////////
template<typename View, typename P, bool IsContainer,
         bool IsPartitionedView, bool IsInverse>
struct default_metadata_projection
{
  typedef implicit_metadata_projection<const View, P> type;
};

// a non-segmented view over a container that does not have an inverse defined
template <typename View, typename P>
struct default_metadata_projection<View, P, true, false, false>
{
  typedef container_metadata_projection<const View, P> type;
};

// a non-segmented view over a container that has an inverse defined
template <typename View, typename P>
struct default_metadata_projection<View, P, true, false, true>
{
  typedef invertible_metadata_projection<const View, P> type;
};

// a segmented view over anything
template <typename View, typename P, bool IsContainer, bool IsInverse>
struct default_metadata_projection<View, P, IsContainer, true, IsInverse>
{
  typedef segmented_metadata_projection<const View, P> type;
};


//////////////////////////////////////////////////////////////////////
/// @brief Traits class that specializes functionality of the coarsening
///        process.
//////////////////////////////////////////////////////////////////////
template<typename View>
struct coarsening_traits
{
  template<typename Part>
  struct construct_projection
  {
    typedef typename default_metadata_projection<
      View, Part,
      is_container<typename View::view_container_type>::value,
      is_segmented_view<View>::value,
      is_invertible_view<View>::value
    >::type type;
  };
};

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_COARSENING_TRAITS_HPP
