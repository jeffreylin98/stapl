/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

//////////////////////////////////////////////////////////////////////
/// @file
/// Common functionality used by both @ref slices_projection and
/// @ref sliced_projection.
//////////////////////////////////////////////////////////////////////

#ifndef STAPL_VIEWS_METADATA_SLICE_COMMON_HPP
#define STAPL_VIEWS_METADATA_SLICE_COMMON_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/apply.hpp>
#include <stapl/utility/tuple/discard.hpp>
#include <stapl/utility/pack_ops.hpp>

namespace stapl {

namespace metadata {

namespace slice_common_impl {

// Test if a whole slice is contained in a single partition
struct is_single_partition_unpacked
{
  template <typename Dim0, typename... Dims>
  Dim0 operator() (Dim0 dim0, Dims... dims) const
  {
    return pack_ops::functional::and_(dim0 == 1, (dims == 1)...);
  }
};

template<typename T>
bool is_single_partition(T slice_part_dims)
{
  return slice_part_dims == 1;
}

template<typename... Ts>
bool is_single_partition(tuple<Ts...> const& slice_part_dims)
{
  return tuple_ops::apply( is_single_partition_unpacked(), slice_part_dims );
}

} // namespace slice_common_impl

//////////////////////////////////////////////////////////////////////
/// @brief Check if each slice is fully contained in a single partition.
/// @param part_dims_orig Number of partitions in each dimension of the
///        original container being sliced.
//////////////////////////////////////////////////////////////////////
template<typename ViewSlices, typename PartDim>
bool part_has_full_slice(PartDim const& part_dims_orig)
{
  return slice_common_impl::is_single_partition(
    tuple_ops::discard<ViewSlices>(part_dims_orig));
}

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_SLICE_COMMON_HPP
