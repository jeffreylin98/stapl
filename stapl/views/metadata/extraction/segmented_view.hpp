/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_EXTRACTION_SEGMENTED_VIEW_HPP
#define STAPL_VIEWS_METADATA_EXTRACTION_SEGMENTED_VIEW_HPP

#include <stapl/views/metadata/extraction/extract_metadata.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Functor to extract the locality metadata for a view whose
///        elements are subviews (@ref segmented_view).
///
/// @tparam Container Container associated with the view that is segmented.
///         Requires that this container reflects a type called container_type,
///         which is the view that is being segmented. This type will most
///         likely be a @ref view_impl::view_container.
//////////////////////////////////////////////////////////////////////
template<typename Container>
struct segmented_view_extractor
{
private:
  /// Type of the container whose elements are subviews. Most likely
  /// a @ref view_impl::view_container
  using container_type = Container;

  /// Metadata entries are linearized, so the index is a scalar
  using index_type = std::size_t;

  /// Type of the view that is being segmented
  using underlying_container_type =
    typename container_type::view_container_type;

  /// Type of the extractor for the view that is being segmented
  using extractor_t = extract_metadata<underlying_container_type>;

public:
  /// Metadata container that is generated from metadata extraction of
  /// the underlying view
  using return_type = typename extractor_t::return_type;

  return_type operator()(Container* ct) const
  {
    return extractor_t()(
      const_cast<underlying_container_type*>(ct->get_view()));
  }
};

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_EXTRACTION_SEGMENTED_VIEW_HPP
