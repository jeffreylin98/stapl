/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_UTILITY_INVOKER_HPP
#define STAPL_VIEWS_METADATA_UTILITY_INVOKER_HPP

#include <stapl/containers/type_traits/index_bounds.hpp>
#include <stapl/utility/tuple.hpp>
#include <stapl/utility/pack_ops.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Invokes an arbitrary functor with a @ref p_object as a parameter.
///
/// @tparam O The p_object type
/// @tparam F The function object type
//////////////////////////////////////////////////////////////////////
template<typename O, typename F>
class invoker
 : public p_object, private F
{
  using object_type = O;

  //////////////////////////////////////////////////////////////////////
  /// @brief Deletes the invoker.
  //////////////////////////////////////////////////////////////////////
  struct destroy
  {
    invoker* const m_invoker;

    explicit destroy(invoker& i) noexcept
    : m_invoker(&i)
    { }

    ~destroy(void)
    { delete m_invoker; }
  };

  //////////////////////////////////////////////////////////////////////
  /// @brief Invoke the function on the resolved @ref p_object.
  //////////////////////////////////////////////////////////////////////
  typename std::result_of<F(object_type const&)>::type
  operator()(object_type const& obj)
  {
    return static_cast<F const&>(*this)(obj);
  }



public:
  invoker(F const& f = F()) : F(f) { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Resolve the handle of the @ref p_object and call the function
  ///        object on the pointer to the resolved @ref p_object.
  //////////////////////////////////////////////////////////////////////
  auto invoke(rmi_handle::reference const& handle)
    -> decltype(this->operator()(std::declval<object_type const&>()))
  {
    auto obj = resolve_handle<object_type>(handle);

    stapl_assert(obj != nullptr, "Handle of object not resolved in invoker");

    return this->operator()(*obj);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Resolve the handle of the @ref p_object and call the function
  ///        object on the pointer to the resolved @ref p_object and delete the
  ///        invoker after that.
  //////////////////////////////////////////////////////////////////////
  auto invoke_and_delete(rmi_handle::reference const& handle)
    -> decltype(this->operator()(std::declval<object_type const&>()))
  {
    destroy d{*this};
    return invoke(handle);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Create an rmi_handle reference to an invoker object that has
///        been constructed on the same set of locations as a given
///        p_object.
///
/// @param obj The p_object whose gang will be used to create the invoker
///            object.
/// @param f   A unary function whose argument is a p_object.
///
/// @return A reference to an rmi_handle of an @see invoker object.
//////////////////////////////////////////////////////////////////////
template<typename O, typename F>
rmi_handle::reference make_invoker(O const& obj, F&& f)
{
  using invoker_type = invoker<O, typename std::decay<F>::type>;

  auto handle_future = construct<invoker_type>(
    obj.get_rmi_handle(), all_locations, std::forward<F>(f)
  );

  return handle_future.get();
}

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_UTILITY_INVOKER_HPP
