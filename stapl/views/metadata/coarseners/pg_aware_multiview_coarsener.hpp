/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_COARSENERS_PG_AWARE_MULTIVIEW_COARSENER_HPP
#define STAPL_VIEWS_METADATA_COARSENERS_PG_AWARE_MULTIVIEW_COARSENER_HPP

#include <stapl/views/metadata/coarseners/multiview.hpp>

namespace stapl {

template<typename T, typename ...OptionalParams>
class nested_pg_view_subview;

template<typename T, typename... OptionalParams>
class nested_parent_pg_view_subview;

template<typename View, bool isView = is_view<View>::value>
struct pg_aware_multiview_coarsener_impl2
{
  static
  typename std::decay<
    decltype(
      get<0>(multiview_coarsener<true>()(tuple<View>(std::declval<View>()))))
  >::type
  apply(View const& view)
  {
    return get<0>(multiview_coarsener<true>()(tuple<View>(view)));
  }
};

template <typename View>
struct pg_aware_multiview_coarsener_impl2<View, false>
{
  static View apply(View const& view)
  { return view; }
};

template<typename Views,
         typename = make_index_sequence<tuple_size<Views>::value>>
struct pg_aware_multiview_coarsener_impl;

template<typename ...Args, std::size_t... Indices>
struct pg_aware_multiview_coarsener_impl<
  tuple<Args...>, index_sequence<Indices...>>
{
  template<typename View>
  static
  auto apply(View const& views)
  STAPL_AUTO_RETURN((
    make_tuple(pg_aware_multiview_coarsener_impl2<Args>::apply(
      get<Indices>(views))...)
  ))
};

//////////////////////////////////////////////////////////////////////
/// @brief coarsening that doesn't apply coarsener on the views coming from
/// paragraph results
//////////////////////////////////////////////////////////////////////
struct pg_aware_multiview_coarsener
{
  template<typename Views>
  auto operator()(Views const& views) const
  STAPL_AUTO_RETURN((
    pg_aware_multiview_coarsener_impl<Views>::apply(views)
  ))
};
} // namespace stapl

#endif // STAPL_VIEWS_METADATA_COARSENERS_PG_AWARE_MULTIVIEW_COARSENER_HPP
