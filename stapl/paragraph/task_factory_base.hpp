/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PARAGRAPH_TASK_FACTORY_BASE_HPP
#define STAPL_PARAGRAPH_TASK_FACTORY_BASE_HPP

#ifndef PARAGRAPH_MAX_VIEWS
#  define PARAGRAPH_MAX_VIEWS 5
#endif

#include <array>

#include <stapl/skeletons/utility/view_index_partition.hpp>
#include <stapl/runtime/p_object.hpp>

#include "incremental_wf.hpp"
#include "factory_wf.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Intermediate class of the task factory hierarchy that provides the
///   edge_mf_type for all factories that derive from it.
/// @ingroup paragraph
//////////////////////////////////////////////////////////////////////
class task_factory_base
 : public p_object,
   public incremental_wf,
   public factory_wf
{
private:
  /// @brief Iterators set to local index spaces of the views to be processed.
  std::array<view_index_iterator_base*, PARAGRAPH_MAX_VIEWS> m_iterators;

  /// @brief Stores whether m_iterators has been initialized.
  bool                                                       m_initialized;

protected:
  /// Stores whether the derived factory has finished specifying tasks.
  bool                                                       m_finished;

  bool initialized(void) const
  {
    return m_initialized;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Reset the iterators over the view indices on this location.
  ///
  /// This is used in factories where multiple tasks will be generated for each
  /// view element to be processed.
  //////////////////////////////////////////////////////////////////////
  void reset_view_indices()
  {
    for (auto&& iter : m_iterators)
      if (iter != nullptr)
        iter->reset();
  }

  void set_view_index_iterator(std::size_t idx, view_index_iterator_base* ptr)
  {
    m_initialized    = true;
    m_iterators[idx] = ptr;
  }

  std::size_t view_indices_size() const
  {
    return m_iterators[0]->size();
  }

  view_index_iterator_base*
  get_view_index_iterator(std::size_t n) const
  {
    stapl_assert(m_iterators[n] != nullptr,
      "get_view_index_iterator(), null pointer at i pos");

    return m_iterators[n];
  }

public:
  task_factory_base(bool b_incremental_generation = false)
    : m_initialized(false),
      m_finished(!b_incremental_generation)
  {
    for (auto&& iter : m_iterators)
      iter = nullptr;
  }

  ~task_factory_base() override
  {
    for (auto&& iter : m_iterators)
      delete iter;
  }

  bool finished() const
  {
    return m_finished;
  }

  virtual void reset()
  { }

  void define_type(typer &t)
  {
    t.transient(m_initialized, false);
    t.transient(m_finished, false);
  }
}; // class task_factory_base

} // namespace stapl

#endif // STAPL_PARAGRAPH_TASK_FACTORY_BASE_HPP
