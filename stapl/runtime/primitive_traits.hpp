/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_PRIMITIVE_TRAITS_HPP
#define STAPL_RUNTIME_PRIMITIVE_TRAITS_HPP

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Traits for primitives.
///
/// These traits document the nature of the primitives and their communication
/// characteristics.
///
/// @see instrumentation
/// @ingroup instrumentation
//////////////////////////////////////////////////////////////////////
struct primitive_traits
{
  enum
  {
    /// Other primitive.
    other        = 0x0000,
    /// Environment creation primitive.
    environment  = 0x0001,
    /// Communication primitive.
    comm         = 0x0002,
    /// Synchronization primitive.
    sync         = 0x0004,
    /// Yielding primitive.
    yield        = 0x0008,
    /// Point-to-point primitive.
    p2p          = 0x0010,
    /// Point-to-many primitive.
    p2m          = 0x0020,
    /// Collective primitive.
    coll         = 0x0040,
    /// Blocking primitive.
    blocking     = 0x0100,
    /// Non-blocking primitive.
    non_blocking = 0x0200,
    /// Ordered primitive.
    ordered      = 0x1000,
    /// Ordered primitive.
    unordered    = 0x2000
  };
};

} // namespace runtime

} // namespace stapl

#endif
