/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_TYPE_TRAITS_IS_CONTIGUOUS_ITERATOR_HPP
#define STAPL_RUNTIME_TYPE_TRAITS_IS_CONTIGUOUS_ITERATOR_HPP

#include <cstddef>
#include <iterator>
#include <type_traits>

#include <string>
#include <valarray>
#include <vector>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief If @p T is an iterator to a container that its elements are in
///        contiguous space it is equivalent to @c std::true_type, for any other
///        iterator it is @c std::false_type.
///
/// @ingroup runtimeTypeTraits
///
/// @todo @c std::array iterator recognition depends on the fact that it is
///       probably a pointer.
/// @todo @c std::vector iterator recognition will fail for vectors with a non
///       default allocator.
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_contiguous_iterator
: public std::integral_constant<
           bool,
           (std::is_pointer<T>::value ||
            std::is_same<
              T,
              decltype(
                std::begin(
                  std::declval<
                    std::valarray<typename std::iterator_traits<T>::value_type>
                  >()
                )
              )
            >::value                  ||
            std::is_same<
              T,
              typename std::vector<
                typename std::iterator_traits<T>::value_type
              >::const_iterator
            >::value                  ||
            std::is_same<
              T,
              typename std::vector<
                typename std::iterator_traits<T>::value_type
              >::iterator
            >::value                  ||
         std::is_same<
              T,
              std::string::const_iterator
            >::value                  ||
            std::is_same<
              T,
              std::string::iterator
            >::value)
         >
{ };

} // namespace runtime

} // namespace stapl

#endif
