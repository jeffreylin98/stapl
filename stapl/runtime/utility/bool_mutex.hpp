/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_BOOL_MUTEX_HPP
#define STAPL_RUNTIME_UTILITY_BOOL_MUTEX_HPP

#include "../exception.hpp"

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Bool based mutex.
///
/// This is an object that follows the concept of a @c std::mutex however it
/// does not guarantee thread safety. Example use case is protecting against
/// function re-entrancy.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
class bool_mutex
{
private:
  bool m_locked;

public:
  constexpr bool_mutex(void)
  : m_locked(false)
  { }

  bool_mutex(bool_mutex const&) = delete;
  bool_mutex& operator=(bool_mutex const&) = delete;

  void lock(void)
  {
    STAPL_RUNTIME_ASSERT(!m_locked);
    m_locked = true;
  }

  void unlock(void)
  {
    STAPL_RUNTIME_ASSERT(m_locked);
    m_locked = false;
  }

  bool try_lock(void) noexcept
  {
    if (m_locked)
      return false;
    m_locked = true;
    return true;
  }

  bool is_locked(void) const noexcept
  { return m_locked; }
};

} // namespace runtime

} // namespace stapl

#endif
