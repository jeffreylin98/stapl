/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_ALGORITHM_HPP
#define STAPL_RUNTIME_UTILITY_ALGORITHM_HPP

#include "../exception.hpp"
#include <climits>
#include <iterator>
#include <unordered_set>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Finds index of the given element in a range.
///
/// @tparam Range Range type.
/// @tparam T     Element type.
///
/// @warning This function has \f$O(n)\f$ time complexity.
///
/// @param r Range to find the element in.
/// @param v Element to be found.
///
/// @return The index of @p v in @p r. If not found, returns @c r.size().
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename Range, typename T>
auto find_index(Range const& r, T const& v) noexcept
  -> typename std::iterator_traits<decltype(std::begin(r))>::difference_type
{
  typename std::iterator_traits<decltype(std::begin(r))>::difference_type i = 0;
  for (auto&& t : r) {
    if (t==v)
      return i;
    ++i;
  }
  return ++i;
}


//////////////////////////////////////////////////////////////////////
/// @brief Returns @c true if the range has unique elements, otherwise returns
///        @c false.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename Range>
bool all_unique(Range const& r)
{
  typedef typename std::iterator_traits<
            decltype(std::begin(r))
          >::value_type value_type;

  std::unordered_set<value_type> s;
  for (auto&& t : r) {
    if (!s.insert(t).second)
      return false;
  }
  return true;
}


//////////////////////////////////////////////////////////////////////
/// @brief Log2 algorithm for unsigned integral types.
///
/// From http://graphics.stanford.edu/~seander/bithacks.html
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename UIntType>
UIntType integral_ceil_log2(UIntType i) noexcept
{
  const UIntType t = ((i & (i-1)) ? 1 : 0); // find if power-of-2
  UIntType r = 0;
  while (i>>=1)
    ++r;
  return (r + t);
}


//////////////////////////////////////////////////////////////////////
/// @brief Sets the [@p msb, @p lsb] bits of @p dst to value @p v and returns
///        the new @p dst.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename UIntType, typename Size>
UIntType set_bits(UIntType dst, UIntType v, Size msb, Size lsb) noexcept
{
  STAPL_RUNTIME_ASSERT(msb>=lsb && (sizeof(UIntType) * CHAR_BIT)>msb);
  const Size len      = (msb - lsb + 1); // inclusive range
  const UIntType mask = ((UIntType(0x1) << len) - UIntType(0x1)) << lsb;
  return ((dst & ~mask) | ((v << lsb) & mask));
}


//////////////////////////////////////////////////////////////////////
/// @brief Reads the [@p msb, @p lsb] bits of @p dst.
///
/// @ingroup runtimeUtility
//////////////////////////////////////////////////////////////////////
template<typename UIntType, typename Size>
UIntType read_bits(UIntType dst, Size msb, Size lsb) noexcept
{
  STAPL_RUNTIME_ASSERT(msb>=lsb && (sizeof(UIntType) * CHAR_BIT)>msb);
  const Size len      = (msb - lsb + 1); // inclusive range
  const UIntType mask = ((UIntType(0x1) << len) - UIntType(0x1));
  return ((dst >> lsb) & mask);
}

} // namespace runtime

} // namespace stapl

#endif
