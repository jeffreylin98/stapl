/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_SERIALIZATION_BASIC_HPP
#define STAPL_RUNTIME_SERIALIZATION_BASIC_HPP

#include "typer_traits.hpp"
#include "../type_traits/is_basic.hpp"
#include <cstring>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typer_traits for basic types.
///
/// @tparam T Object type to be packed.
///
/// @see is_basic
/// @ingroup serialization
//////////////////////////////////////////////////////////////////////
template<typename T>
class typer_traits<T,
                   typename std::enable_if<is_basic<T>::value>::type>
{
public:
  using value_type = T;

  static constexpr std::size_t packed_size(T const&) noexcept
  { return 0; }

  static constexpr std::pair<bool, std::size_t>
  meets_requirements(const typer::pass_type, T const&) noexcept
  { return std::make_pair(true, std::size_t(0)); }

  static void prepack(T* dest,
                      T const* src,
                      const std::size_t num = 1) noexcept
  {
    if (std::is_empty<T>::value)
      return;
    std::memcpy(static_cast<void*>(dest),
                static_cast<void const*>(src),
                (sizeof(T) * num));
  }

  static constexpr std::size_t pack(T&,
                                    void*,
                                    const std::size_t,
                                    T const&) noexcept
  { return 0; }

  static constexpr std::size_t unpack(T&, void*) noexcept
  { return 0; }

  static void destroy(T&) noexcept
  { }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref typer_traits_specialization for basic types.
///
/// @see is_basic
/// @ingroup serialization
//////////////////////////////////////////////////////////////////////
template<typename T>
struct typer_traits_specialization<T,
                                   typename std::enable_if<
                                     is_basic<T>::value
                                   >::type>
: public std::true_type
{ };

} // namespace stapl

#endif
