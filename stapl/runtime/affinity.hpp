/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_AFFINITY_HPP
#define STAPL_RUNTIME_AFFINITY_HPP

#include "config.hpp"
#include "type_traits/is_basic.hpp"
#include <iosfwd>
#include <limits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Describes the affinity of a location.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
struct affinity_tag
{
  typedef runtime::internal_affinity_tag tag_type;

  static const std::size_t num_bits;

  static constexpr affinity_tag make_tag(const tag_type t) noexcept
  { return { t }; }

  tag_type tag;
};


constexpr bool operator==(const affinity_tag x, const affinity_tag y) noexcept
{
  return (x.tag==y.tag);
}

constexpr bool operator!=(const affinity_tag x, const affinity_tag y) noexcept
{
  return !(x==y);
}

std::ostream& operator<<(std::ostream&, const affinity_tag);


//////////////////////////////////////////////////////////////////////
/// @brief Invalid affinity tag.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
constexpr affinity_tag invalid_affinity_tag =
  { std::numeric_limits<affinity_tag::tag_type>::max() };

} // namespace stapl

STAPL_IS_BASIC_TYPE(stapl::affinity_tag)

#endif
