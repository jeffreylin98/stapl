/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_THIS_CONTEXT_HPP
#define STAPL_RUNTIME_THIS_CONTEXT_HPP

#include "context_id.hpp"
#include <boost/optional.hpp>

namespace stapl {

namespace runtime {

class context;
class location_md;


//////////////////////////////////////////////////////////////////////
/// @brief STAPL Runtime System RMI execution context management.
//////////////////////////////////////////////////////////////////////
namespace this_context {

//////////////////////////////////////////////////////////////////////
/// @brief Pushes a base @ref context on the stack.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void push_base(context&);


//////////////////////////////////////////////////////////////////////
/// @brief Pops the base @ref context from the stack.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void pop_base(context&);


//////////////////////////////////////////////////////////////////////
/// @brief Pushes a placeholder for a new @ref context on the stack.
///
/// @see gang
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void push_placeholder(boost::optional<context>&);


//////////////////////////////////////////////////////////////////////
/// @brief Pops the placeholder from the stack.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void pop_placeholder(void);


//////////////////////////////////////////////////////////////////////
/// @brief Switches to the base @ref context of @p l.
///
/// The location metadata @p l will be used to either create a new base context
/// or to switch to an existing one.
///
/// @see gang
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void switch_to(location_md&, boost::optional<context>&);


//////////////////////////////////////////////////////////////////////
/// @brief Unswitches from the @ref context on the stack.
///
/// @see gang
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
void unswitch(void);


//////////////////////////////////////////////////////////////////////
/// @brief Returns the current @ref context from the stack.
///
/// If the @ref context creation was deferred (e.g. one location gangs defer
/// the creation of all metadata) then this function will create all the
/// required metadata and return the associated @ref context object.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
context& get(void);

//////////////////////////////////////////////////////////////////////
/// @brief Returns the @ref base context of the context at the top of
///        the stack.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
context& base_of_top(void);


//////////////////////////////////////////////////////////////////////
/// @brief Returns a pointer to the current @ref context from the stack.
///
/// If the @ref context creation was deferred, then it returns @c nullptr.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
context* try_get(void) noexcept;


//////////////////////////////////////////////////////////////////////
/// @brief Returns the current context id from the stack.
///
/// If the @ref context creation was deferred (e.g. one location gangs defer
/// the creation of all metadata) then this function will create all the
/// required metadata and return the associated @ref context object.
///
/// @see this_context::get()
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
context_id const& get_id(void);


//////////////////////////////////////////////////////////////////////
/// @brief Returns a pointer to the location metadata of the given gang id if
///        it is in the stack, otherwise @c nullptr.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
location_md* try_get_location_md(const gang_id) noexcept;


//////////////////////////////////////////////////////////////////////
/// @brief Returns @c true if the execution can be restored for location @p l.
///
/// @warning This is an expensive operation that restores an SPMD section, used
///          in @ref restore().
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
bool can_restore(location_md& l);

} // namespace this_context

} // namespace runtime

} // namespace stapl

#endif
