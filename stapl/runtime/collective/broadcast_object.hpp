/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COLLECTIVE_BROADCAST_OBJECT_HPP
#define STAPL_RUNTIME_COLLECTIVE_BROADCAST_OBJECT_HPP

#include "../context.hpp"
#include "../rmi_handle.hpp"
#include "../value_handle.hpp"
#include "../non_rmi/response.hpp"

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Performs a broadcast over all locations of the current gang.
///
/// @tparam T Object type.
///
/// @ingroup runtimeCollectives
///
/// @todo Use platform optimized broadcast implementation.
//////////////////////////////////////////////////////////////////////
template<typename T>
class broadcast_object
: public value_handle<T>
{
public:
  using value_type    = T;
private:
  using response_type = handle_response<packed_handle_type, broadcast_object>;

  friend response_type;

  rmi_handle m_handle;

public:
  explicit broadcast_object(context& ctx)
  : m_handle(ctx, this)
  { }

  rmi_handle::reference const& get_rmi_handle(void) noexcept
  { return m_handle; }

  rmi_handle::const_reference const& get_rmi_handle(void) const noexcept
  { return m_handle; }

private:
  using value_handle<T>::set_value;

public:
  void operator()(T const& t)
  { response_type{}(m_handle, t); }

  void operator()(T&& t)
  { response_type{}(m_handle, std::move(t)); }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref broadcast_object for @c void.
///
/// @ingroup runtimeCollectives
//////////////////////////////////////////////////////////////////////
template<>
class broadcast_object<void>
: public value_handle<void>
{
public:
  using value_type    = void;
private:
  using response_type = handle_response<packed_handle_type, broadcast_object>;

  friend response_type;

  rmi_handle m_handle;

public:
  explicit broadcast_object(context& ctx)
  : m_handle(ctx, this)
  { }

  rmi_handle::reference const& get_rmi_handle(void) noexcept
  { return m_handle; }

  rmi_handle::const_reference const& get_rmi_handle(void) const noexcept
  { return m_handle; }

private:
  using value_handle<void>::set_value;

public:
  void operator()(void)
  { response_type{}(m_handle); }
};

} // namespace runtime

} // namespace stapl

#endif
