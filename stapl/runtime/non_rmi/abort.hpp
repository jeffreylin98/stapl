/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_NON_RMI_ABORT_RMI_HPP
#define STAPL_RUNTIME_NON_RMI_ABORT_RMI_HPP

#include <sstream>
#include <string>

namespace stapl {

////////////////////////////////////////////////////////////////////
/// @brief Displays the given @c std::string and aborts execution.
///
/// @ingroup ARMI
////////////////////////////////////////////////////////////////////
void abort(std::string const&);


////////////////////////////////////////////////////////////////////
/// @brief Aborts execution.
///
/// @ingroup ARMI
////////////////////////////////////////////////////////////////////
inline void abort(void)
{
  abort(std::string{});
}


////////////////////////////////////////////////////////////////////
/// @brief Outputs the given object to @c std::cerr as a string and aborts
///        execution.
///
/// @ingroup ARMI
////////////////////////////////////////////////////////////////////
template<typename T>
void abort(T const& t)
{
  std::ostringstream oss;
  oss << t;
  abort(oss.str());
}

} // namespace stapl

#endif
