/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_NON_RMI_EXTERNAL_HPP
#define STAPL_RUNTIME_NON_RMI_EXTERNAL_HPP

#include "../context.hpp"
#include "../instrumentation.hpp"
#include "../runqueue.hpp"
#include "../collective/barrier_object.hpp"
#include <functional>
#include <set>
#include <type_traits>
#include <utility>
#include <boost/optional.hpp>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Function object that calls a function passed from
///        @ref external_call().
///
/// @tparam R Function result type.
/// @tparam T Argument types.
///
/// It performs a barrier to make sure that all locations are on the same point
/// in execution and then locks the communication library so that no other
/// primitive can use it.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<typename R>
struct external_caller
{
  using result_type = boost::optional<R>;

  template<typename F, typename... T>
  static result_type apply(F&& f, T&&... t)
  {
    auto* const ctx = this_context::try_get();
    if (ctx) {
      // wait for all locations to arrive
      barrier_object b{*ctx};
      b();
      b.wait();

      // only the leader will call f
      if (!ctx->get_location_md().is_leader())
        return boost::optional<R>{};
    }

    // call the function
    runqueue::lock_type lock;
    return boost::optional<R>{f(std::forward<T>(t)...)};
  }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref external_caller for @c void return type.
///
/// @ingroup requestBuildingBlock
//////////////////////////////////////////////////////////////////////
template<>
struct external_caller<void>
{
  using result_type = bool;

  template<typename F, typename... T>
  static result_type apply(F&& f, T&&... t)
  {
    auto* const ctx = this_context::try_get();
    if (ctx) {
      // wait for all locations to arrive
      barrier_object b{*ctx};
      b();
      b.wait();

      // only the leader will call f
      if (!ctx->get_location_md().is_leader())
        return false;
    }

    // call the function
    runqueue::lock_type lock;
    f(std::forward<T>(t)...);
    return true;
  }
};

} // namespace runtime


////////////////////////////////////////////////////////////////////
/// @brief Returns the location ids that are going to make the external call.
///
/// @ingroup ARMI
////////////////////////////////////////////////////////////////////
std::set<unsigned int> external_callers(void);


//////////////////////////////////////////////////////////////////////
/// @brief Calls an external library function.
///
/// @tparam F Function type.
/// @tparam T Argument types.
///
/// This function is useful for calling functions that are not STAPL-aware or
/// thread-safe, such as MPI-based libraries. It is going to call @p f only from
/// one location per process.
///
/// It is the user's responsibility to call the @ref external_call() in a gang
/// that @p f can be called correctly. Most of the times @ref external_call()
/// should be called in @ref stapl_main().
///
/// @warning Calling any runtime primitive inside @p f is undefined behavior.
///
/// @param f External library function to be called.
/// @param t Arguments to pass to the function.
///
/// @return If @p R is not @c void, the result of @p f(t...) is returned in a
///         @c boost::optional<R> which has a value in all locations that @p f
///         has been called.
///         If @p R is @c void, then it returns @c true in all locations that
///         @p f has been called, otherwise @c false.
///
/// @ingroup ARMI
//////////////////////////////////////////////////////////////////////
template<typename F, typename... T>
typename runtime::external_caller<
  typename std::result_of<F(T...)>::type
>::result_type
external_call(F&& f, T&&... t)
{
  using namespace stapl::runtime;

  STAPL_RUNTIME_PROFILE("external_call()", (primitive_traits::blocking |
                                            primitive_traits::coll     |
                                            primitive_traits::sync));

  using wf_type = external_caller<typename std::result_of<F(T...)>::type>;
  return wf_type::apply(std::forward<F>(f), std::forward<T>(t)...);
}

} // namespace stapl

#endif
