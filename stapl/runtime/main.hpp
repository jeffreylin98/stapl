/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_MAIN_HPP
#define STAPL_RUNTIME_MAIN_HPP

#include "exit_code.hpp"
#include <functional>
#include <utility>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Function object that is used with @ref execute() to initiate a STAPL
///        application.
///
/// @ingroup ARMIUtilities
///
/// @todo It is not yet clear if it should be replaced by a @ref paragraph or
///       not.
//////////////////////////////////////////////////////////////////////
class main_wf
{
private:
  std::function<exit_code(int, char*[])> m_f;
  int                                    m_argc;
  char**                                 m_argv;

public:
  template<typename Function>
  main_wf(int argc, char* argv[], Function&& f)
  : m_f(std::forward<Function>(f)),
    m_argc(argc),
    m_argv(argv)
  { }

  void operator()(void);
};

} // namespace stapl

#endif
