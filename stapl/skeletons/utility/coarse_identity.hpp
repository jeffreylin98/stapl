/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_COARSE_IDENTITY_HPP
#define STAPL_SKELETONS_UTILITY_COARSE_IDENTITY_HPP

#include <vector>
#include <algorithm>
#include <stapl/utility/domain_ops.hpp>
#include <stapl/views/view_traits.hpp>

namespace stapl {

template <typename T, int dims = 1>
struct coarse_identity;


//////////////////////////////////////////////////////////////////////
/// @brief A workaround for the cases that a multidimensional coarsened
/// view is passed to @c stapl::identity<T>. This struct brings a
/// multidimensional coarsened view to @c PARAGRAPH environment.
///
/// @tparam T    type of the fine-grain elements
/// @tparam dims the dimensionality of the input
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
template <typename T, int dims>
struct coarse_identity
{
  typedef std::vector<T> result_type;

  template <typename V>
  result_type operator()(V const& v)
  {
    result_type r;
    r.reserve(v.size());

    std::copy(v.begin(), v.end(), std::back_inserter(r));
    return r;
  }
};

} // namespace stapl

#endif // STAPL_SKELETONS_UTILITY_COARSE_IDENTITY_hpp
