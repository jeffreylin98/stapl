/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_LIGHTWEIGHT_MULTIARRAY_STORAGE_HPP
#define STAPL_SKELETONS_UTILITY_LIGHTWEIGHT_MULTIARRAY_STORAGE_HPP

#include <iterator>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Basic linear storage class used by @ref lightweight_multiarray_base
/// to avoid initialization scan if desired.
///
/// @todo Will only elide initialization for fundamental types at the moment.
/// To extend further, need to use array of aligned_storage, carefully.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct lightweight_multiarray_storage
{
  static_assert(
    std::is_fundamental<T>::value,
    "non-initialized lightweight_multiarray only supports fundamental types.");

public:
  using value_type      = T;
  using size_type       = std::size_t;
  using iterator        = value_type*;
  using const_iterator  = value_type const*;
  using reference       = typename std::add_lvalue_reference<value_type>::type;
  using const_reference = typename std::add_const<reference>::type;

private:
  std::unique_ptr<value_type[]> m_data;
  const size_type               m_size;

public:
  explicit lightweight_multiarray_storage(size_t size)
    : m_data(new value_type[size]),
      m_size(size)
  { }

  reference operator[](size_type n)
  {
    return *(m_data.get() + n);
  }

  iterator begin(void) const
  {
    return iterator{m_data.get()};
  }

  iterator end(void) const
  {
    return iterator{m_data.get() + m_size};
  }

  const_iterator cbegin(void) const
  {
    return begin();
  }

  const_iterator cend(void) const
  {
    return end();
  }

  size_type size(void) const
  {
    return m_size;
  }

  bool empty(void) const
  {
    return m_size == 0;
  }

  void define_type(typer &t)
  {
    t.member(m_size);
    t.member(m_data);
  }
}; // struct lightweight_multiarray_storage

} // namespace stapl

#endif // STAPL_SKELETONS_UTILITY_LIGHTWEIGHT_MULTIARRAY_STORAGE_HPP
