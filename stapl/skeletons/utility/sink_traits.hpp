/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_SINK_TRAITS_HPP
#define STAPL_SKELETONS_UTILITY_SINK_TRAITS_HPP

#include <stapl/skeletons/utility/mappers.hpp>

namespace stapl {
namespace skeletons {


struct location_id
{
  template<typename... Arg>
  int operator()(Arg&&... arg) const
  {
    return 0;
  }
};


template <typename SkeletonTag, int Dim = 2>
struct sink_traits
{
  using domain_type     = indexed_domain<std::size_t, Dim>;
  using index_type      = typename domain_type::index_type;
  using traversal_t     = typename domain_type::traversal_type;
  using linearize_t     = nd_linearize<index_type, traversal_t>;

  using output_to_output_mapper_type = default_output_to_output_mapper<Dim>;
  using output_to_input_mapper_type  = default_output_to_input_mapper<Dim>;
  using input_to_input_mapper_type   = default_input_to_input_mapper<Dim>;
  using result_mapper_type           = linearize_t;
  using should_set_type              = should_flow<SkeletonTag>;
  using should_flow_t                = should_flow<SkeletonTag>;
};


template <int Dim>
struct sink_traits<tags::broadcast_to_locs, Dim>
{
  using domain_type     = indexed_domain<std::size_t, Dim>;
  using index_type      = typename domain_type::index_type;
  using traversal_t     = typename domain_type::traversal_type;
  using linearize_t     = nd_linearize<index_type, traversal_t>;

  using output_to_output_mapper_type = default_output_to_output_mapper<Dim>;
  using output_to_input_mapper_type  = linearize_t;
  using result_mapper_type           = location_id;
  using should_set_type              = spans::per_location;
};


template <int Dim>
struct sink_traits<tags::expand_from_pow_two, Dim>
{
  using domain_type     = indexed_domain<std::size_t, Dim>;
  using index_type      = typename domain_type::index_type;
  using traversal_t     = typename domain_type::traversal_type;
  using linearize_t     = nd_linearize<index_type, traversal_t>;

  using output_to_output_mapper_type = default_output_to_output_mapper<Dim>;
  using output_to_input_mapper_type  = linearize_t;
  using result_mapper_type           = location_id;
  using should_set_type              = spans::per_location;
};

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_UTILITY_SINK_TRAITS_HPP
