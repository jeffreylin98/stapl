/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_UTILITY_LAZY_SIZES_HPP
#define STAPL_SKELETONS_UTILITY_LAZY_SIZES_HPP

namespace stapl {
namespace skeletons {

//////////////////////////////////////////////////////////////////////
/// @brief Log lazy size is used in skeletons where the size is
/// defined as the logarithm of the inputs. A good example is an
/// n-ary @c tree
///
/// @tparam i the arity of the tree
///
/// @see tree
/// @see repeat
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
class input_lazysize
{
public:
  std::size_t operator()(std::size_t a)
  {
    return a == 1 ? 1 : a-1;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Log lazy size is used in skeletons where the size is
/// defined as the logarithm of the inputs. A good example is an
/// n-ary @c tree
///
/// @tparam i the arity of the tree
///
/// @see tree
/// @see repeat
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
template <int i>
class log_lazysize
{
public:
  std::size_t operator()(std::size_t a)
  {
    return a == 1 ? 1 : static_cast<std::size_t>(log(a) / log(i));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Log ceiling lazy size is used in skeletons where the size is
/// defined as the ceiling of the logarithm of the input size.
/// Hillis Steele or any other pointer jumping algorithm use this lazy
/// size evaluator.
///
/// @tparam i the arity of the tree
///
/// @see log_lazysize
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
template <int i>
class log_ceil_lazysize
{
public:
  std::size_t operator()(std::size_t a)
  {
    return a == 1 ? 1 : static_cast<std::size_t>(ceil(log(a) / log(i)));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief The basic lazy size evaluator which returns a predefined
/// size upon evaluation.
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
class lazysize
{
  const std::size_t m_const_size;

public:
  explicit lazysize(std::size_t const_size)
    : m_const_size(const_size)
  { }

  template <typename T>
  std::size_t operator()(T&&)
  {
    return m_const_size;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief The lazy size evaluator for matrix which returns a predefined
/// size upon evaluation.
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
struct matrix_lazysize
{
  template <typename SizeF>
  std::size_t operator()(SizeF&& mx_size)
  {
    return stapl::get<1>(mx_size);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Lazy size evaluation for divide-and-conquer algorithms
///
/// @ingroup skeletonsUtilities
//////////////////////////////////////////////////////////////////////
class lazy_divide_and_conquer_size
{
public:
  std::size_t operator()(std::size_t a)
  {

    if (a == 1) {
      return 1;
    }
    else {
      std::size_t reps = static_cast<std::size_t>(ceil(log(a) / log(2)));
      std::size_t result = (reps*(reps+1))/2;
      return result;
    }
  }
};


}
}
#endif // STAPL_SKELETONS_UTILITY_LAZY_SIZES_HPP
