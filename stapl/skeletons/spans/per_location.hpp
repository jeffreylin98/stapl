/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_PER_LOCATION_HPP
#define STAPL_SKELETONS_SPANS_PER_LOCATION_HPP

#include <stapl/skeletons/spans/balanced.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/utility/tuple/tuple.hpp>

namespace stapl {
namespace skeletons {
namespace spans {

//////////////////////////////////////////////////////////////////////
/// @brief A balanced span (@see balanced) in which each location
/// will have exactly one element to spawn.
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
class per_location
  : public balanced<1>
{
public:
  using size_type = balanced<>::size_type;
  using dimension_type = balanced<>::dimension_type;

  template <bool forced = false, typename Spawner, typename... Views>
  void set_size(Spawner const& spawner, Views const&... views)
  {
    if (forced)
      forced_set_size(spawner, views...);
    else
      balanced::set_size(spawner,
                         stapl::counting_view<int>(spawner.get_num_PEs()));
  }

  template <typename Spawner, typename... Views>
  void forced_set_size(Spawner const& spawner, Views const&... views)
  {
    using VDomains =
      stapl::tuple<skeletons::domain_type<typename Views::domain_type,
                                          has_finite_domain<Views>::value>...>;
    auto view_domains =
      stapl::get<skeletons::first_finite_domain_index<VDomains>::value>(
        make_tuple(skeletons::domain_type<typename Views::domain_type,
                                          has_finite_domain<Views>::value>(
          views.domain())...));

    if (view_domains.size() < spawner.get_num_PEs()) {
      balanced::set_size(spawner, views...);
    } else {
      balanced::set_size(spawner,
                         stapl::counting_view<int>(spawner.get_num_PEs()));
    }
  }
};

} // namespace spans
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_SPANS_PER_LOCATION_HPP
