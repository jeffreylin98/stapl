/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_OPTIMIZERS_ZIP_HPP
#define STAPL_SKELETONS_OPTIMIZERS_ZIP_HPP

#include <type_traits>
#include <vector>
#include <stapl/skeletons/transformations/optimizer.hpp>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/utility/skeleton.hpp>
#include <stapl/skeletons/utility/dynamic_wf.hpp>
#include <stapl/skeletons/transformations/optimizers/utils.hpp>
#include <stapl/skeletons/transformations/optimizers/zip_traversals.hpp>
#include <stapl/views/type_traits/is_identity.hpp>
#include <stapl/views/type_traits/is_segmented_view.hpp>

namespace stapl {
namespace skeletons {
namespace optimizers {

template <typename SkeletonTag, typename ExecutionTag>
struct optimizer;

//////////////////////////////////////////////////////////////////////
/// @brief A @c zip optimizer is used in the cases that all views are
/// local and their traversal would be fast which improves the
/// performance. You have to notice that the inner vector creation
/// takes some time and you have to consider it before using this
/// optimizer.
///
/// @ingroup skeletonsTransformationsCoarse
//////////////////////////////////////////////////////////////////////
template <typename Tag, int arity>
struct optimizer<tags::zip<Tag, arity>, tags::sequential_execution>
{
  template <typename R>
  struct result;

  template <typename Optimizer, typename OutputValueType>
  struct result<Optimizer(OutputValueType)>
  {
    using type = std::vector<OutputValueType>;
  };

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Sequential zip optimizer dispatches the execution to
  /// two different signatures based on the requirement of the given
  /// zip operation. If the zip operations needs the @ref paragraph_view,
  /// the dynamic specialization is called, otherwise the non_dynamic
  /// one is called.
  ///
  /// @return the result of the zip operation (can be void)
  //////////////////////////////////////////////////////////////////////
  template <typename R, typename S, typename... Args>
  static R execute(S&& skeleton, Args&&... args)
  {
    using zipper_t = typename std::conditional<
      helpers::pack_has_iterator<typename std::decay<Args>::type...>::value,
      zipper_iterator, zipper_domain
    >::type;

    using dispatcher_t = typename std::conditional<
                           std::is_base_of<
                             dynamic_wf,
                             typename std::decay<S>::type::op_type>::value,
                           typename zipper_t::dynamic,
                           typename zipper_t::non_dynamic>::type;


    return dispatcher_t::template execute<R>(
             std::forward<S>(skeleton),
             std::forward<Args>(args)...);
  }
};


} // namespace optimizers
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_OPTIMIZERS_ZIP_HPP
