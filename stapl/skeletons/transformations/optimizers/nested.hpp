/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_OPTIMIZERS_NESTED_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_OPTIMIZERS_NESTED_HPP

#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/transformations/optimizer.hpp>

namespace stapl {
namespace skeletons {
namespace optimizers {

template <typename SkeletonTag, typename ExecutionTag>
struct optimizer;

//////////////////////////////////////////////////////////////////////
/// @brief A @c nested optimizer will spawn the skeleton in parallel
/// allowing nested parallelism to be defined and exploited in a high
/// level.
///
/// @see skeletons::execute
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename SkeletonTag, typename ExecutionTag>
struct optimizer;


template <typename Tag>
struct optimizer<Tag, tags::nested_execution<false>>
{
  // would be discarded if a deriving optimizer redefines result_type
  template <typename R>
  struct result;

  template <typename Optimizer, typename OutputValueType>
  struct result<Optimizer(OutputValueType)>
  {
    using traits_t = stapl::skeletons::skeleton_execution_traits<
                       Tag, tags::nested_execution<false>>;
    using type = typename traits_t::template result_type<OutputValueType>;
  };
};

//////////////////////////////////////////////////////////////////////
/// @brief An @c optimizer with the nested execution strategy, spawns
/// the given skeleton (creates a data flow graph).
///
/// The value 'true' in default_execution denotes that the given
/// skeleton is reducing the input to a single value. In order to
/// provide the reduced value to all the participating locations,
/// the given skeleton needs to be composed with a @c broadcast_to_locs
/// skeleton, and should then be wrapped by a @c sink_value skeleton.
///
/// @see skeletons::execute
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename Tag>
struct optimizer<Tag, tags::nested_execution<true>>
{
  template <typename FlowValueType>
  struct result;

  template <typename Optimizer, typename FlowValueType>
  struct result<Optimizer(FlowValueType)>
  {
    using type = FlowValueType;
  };
};


} // namespace optimizers
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_OPTIMIZERS_NESTED_HPP
