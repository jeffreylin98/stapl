/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_NEST_NEST_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_NEST_NEST_HPP

#include <stapl/skeletons/transformations/transform.hpp>
#include <stapl/skeletons/transformations/wrapped_skeleton.hpp>
#include <stapl/skeletons/transformations/optimizers/nested.hpp>

#include <stapl/skeletons/functional/map.hpp>
#include <stapl/skeletons/functional/reduce_to_locs.hpp>
#include <stapl/skeletons/functional/broadcast_to_locs.hpp>

namespace stapl {
namespace skeletons {
namespace transformations {

template <typename S, typename SkeletonTag, typename TransformTag>
struct transform;


//////////////////////////////////////////////////////////////////////
/// @brief The transformation for a @ref skeletons::reduce in a nested
/// section (e.g., map(reduce)) transforms it to a
/// @ref skeletons::reduce_to_locs. In addition, for performance reasons
/// it should also add a map(identity<T>) to avoid remote fetches
/// from a view.
///
/// @see make_paragraph_skeleton_manager
/// @todo The map(identity<T>) can be removed whenever fetches from
/// views are changed to future-then.
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename S, typename Tag>
struct transform<S, tags::reduce<Tag>, tags::nest>
{
private:
  using value_t = typename S::op_type::result_type;

public:
  template <typename ExecutionParams>
  static auto call(S const& skeleton, ExecutionParams const& execution_params)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<true>>(
      skeletons::compose(
        skeletons::map(stapl::identity<value_t>()),
        skeletons::reduce_to_locs<true>(skeleton.get_op())),
        execution_params)
  ))

  static auto call(S const& skeleton)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<true>>(
      skeletons::compose(
        skeletons::map(stapl::identity<value_t>()),
        skeletons::reduce_to_locs<true>(skeleton.get_op())))
  ))
};


//////////////////////////////////////////////////////////////////////
/// @brief The transformation for a @ref skeletons::zip_reduce in a nested
/// section (e.g., zip(reduce)) composes it with a @c broadcast_to_locs
/// skeleton.
///
/// @see make_paragraph_skeleton_manager
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename S, int i>
struct transform<S, tags::zip_reduce<i>, tags::nest>
{
private:
  using value_t   = typename S::reduce_op_type::result_type;

public:
  template <typename ExecutionParams>
  static auto call(S const& skeleton, ExecutionParams const& execution_params)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<true>>(
      skeletons::compose(skeleton, skeletons::broadcast_to_locs<true>()),
      execution_params)
  ))

  static auto call(S const& skeleton)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<true>>(
      skeletons::compose(skeleton, skeletons::broadcast_to_locs<true>()))
  ))
};


//////////////////////////////////////////////////////////////////////
/// @brief In the default case, a transformation of a skeleton with
/// the @c tags::nest tag should not modify the skeleton. For example,
/// a nested @ref skeletons::map or @ref skeletons::zip can be executed
/// without applying any transformations.
///
/// @see make_paragraph_skeleton_manager
///
/// @ingroup skeletonsTransformationsNest
//////////////////////////////////////////////////////////////////////
template <typename S, typename Tag>
struct transform<S, Tag, tags::nest>
{
private:

public:
  template <typename ExecutionParams>
  static auto call(S const& skeleton, ExecutionParams const& execution_params)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<false>>(
      skeleton, execution_params)
  ))

  static auto call(S const& skeleton)
  STAPL_AUTO_RETURN((
    skeletons::wrap<tags::nested_execution<false>>(
      skeleton)
  ))
};

} // namespace transformations


//////////////////////////////////////////////////////////////////////
/// @brief no transformation for non skeleton operators
///
/// @param skeleton     the skeleton to be nested
///
/// @ingroup skeletonsTransformations
//////////////////////////////////////////////////////////////////////
template <typename Op,
          typename =
            typename std::enable_if<
              !is_skeleton<typename std::decay<Op>::type>::value>::type>
Op nest(Op&& op)
{
  return op;
}


//////////////////////////////////////////////////////////////////////
/// @brief recursive calls to the nested skeletons for setting their
///        proper filters and mappers for paragraph ports
///
/// @param skeleton     the skeleton to be nested
///
/// @ingroup skeletonsTransformations
//////////////////////////////////////////////////////////////////////
template <typename S,
          typename =
            typename std::enable_if<
              is_skeleton<typename std::decay<S>::type>::value>::type>
auto
nest(S&& skeleton)
STAPL_AUTO_RETURN((
  skeletons::transform<tags::recursive_nest>(
    std::forward<S>(skeleton))
))


} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_NEST_NEST_HPP
