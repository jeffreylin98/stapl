/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>

#include <iostream>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <stapl/skeletons/map_sched.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/views/array_ro_view.hpp>

using namespace stapl;

template <typename C = view_impl::counting_container<std::size_t, 1,
  view_impl::default_container>,
          typename Dom = typename container_traits<C>::domain_type,
          typename MapFunc = f_ident<typename Dom::index_type>>
struct tester_view
  : array_ro_view<C, Dom, MapFunc>
{
  typedef C                                          view_container_type;
  typedef Dom                                        domain_type;
  typedef MapFunc                                    map_func_type;
  typedef array_ro_view<C, Dom, MapFunc>             base_type;

  tester_view()
  { }

  tester_view(std::size_t num_components)
    : base_type(new view_container_type(
                num_components*get_num_locations(), (std::size_t) 0))
  { }

  tester_view(view_container_type const& vcont, domain_type const& dom,
              map_func_type mfunc=map_func_type(),
              tester_view const& other=tester_view<C>())
    : base_type(vcont, dom, mfunc)
  { }
};


struct empty_wf
{
  typedef void result_type;

  template<typename Reference>
  void operator()(Reference) const
  {
    std::this_thread::sleep_for(std::chrono::microseconds{rand() % 10000});
  }
};


stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (stapl::get_location_id() == 0)
  {
    std::cout << "paragraph termination detection positive with "
              << stapl::get_num_locations() << " locations... ";
    std::srand(std::time(NULL));
  }

  const int nelems = stapl::get_num_locations()*1000;

  stapl::rmi_fence();

  map_func_sched(stapl::default_scheduler(), empty_wf(), tester_view<>(nelems));

  // if we reach here then it means that the test passed
  if (stapl::get_location_id()==0)
    std::cout << "Passed" << std::endl;

  return EXIT_SUCCESS;
}
