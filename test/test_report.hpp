/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_TEST_REPORT_HPP
#define STAPL_TEST_REPORT_HPP

#include <algorithm>
#include <iostream>

#include <stapl/utility/distributed_value.hpp>
#include <stapl/utility/do_once.hpp>

#define STAPL_TEST_MESSAGE(str)                                                \
  stapl::do_once([&](void) { std::cout << str << "\n"; });

#ifdef REPORT_WITH_COLOR
#  define STAPL_TEST_PASS_STRING "\033[;32mPASSED\033[0m"
#  define STAPL_TEST_FAIL_STRING "\033[;31mFAILED\033[0m"
#else
#  define STAPL_TEST_PASS_STRING "PASSED"
#  define STAPL_TEST_FAIL_STRING "FAILED"
#endif

#define STAPL_TEST_ONE_OF_REPORT(expr, vals, str)                              \
  stapl::do_once([&](void) {                                                   \
    std::cout << str;                                                          \
    if (std::count(std::begin(vals), std::end(vals), expr))                    \
      std::cout << ": " << STAPL_TEST_PASS_STRING << "\n";                     \
    else                                                                       \
      std::cout << ": " << STAPL_TEST_FAIL_STRING << "\n";                     \
  });

#define STAPL_TEST_REPORT(val, str)                                            \
  stapl::do_once([&](void) {                                                   \
    std::cout << str;                                                          \
    if (val)                                                                   \
      std::cout << ": " << STAPL_TEST_PASS_STRING << "\n";                     \
    else                                                                       \
      std::cout << ": " << STAPL_TEST_FAIL_STRING << "\n";                     \
  });

#define STAPL_TEST_ALL_LOCATION_REPORT(val, str)                               \
  do {                                                                         \
    const bool passed = stapl::distributed_value<bool>{ val }                  \
                          .reduce(std::logical_and<bool>{})                    \
                          .get();                                              \
    STAPL_TEST_REPORT(passed, str)                                             \
  } while (0)

#endif // STAPL_TEST_REPORT_HPP
