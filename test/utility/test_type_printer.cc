/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime.hpp>
#include <stapl/utility/type_printer.hpp>
#include <iostream>
#include <string>
#include <tuple>
#include <utility>
#include "../test_report.hpp"


template<typename T>
std::string get_type_name(void)
{
  std::ostringstream os;
  os << stapl::type_printer<T>();
  return os.str();
}


template<typename... T>
std::string get_type_names(void)
{
  std::ostringstream os;
  os << stapl::typelist_printer<T...>();
  return os.str();
}


template<typename... T>
std::string get_type_names(T... t)
{
  std::ostringstream os;
  os << stapl::object_type_printer(t...);
  return os.str();
}


void test_type_printer(void)
{
  // primitive types
  STAPL_TEST_REPORT(
    (std::string("int")==get_type_name<int>()),
     "type_printer<int>");
  STAPL_TEST_REPORT(
    (std::string("double")==get_type_name<double>()),
     "type_printer<double>");
  STAPL_TEST_REPORT(
    (std::string("long")==get_type_name<long>()),
     "type_printer<long>");

  // pair
  STAPL_TEST_REPORT(
    (std::string("std::pair<int, int>")==get_type_name<std::pair<int,int>>()),
    "type_printer<std::pair<int,int>>");

  // tuple
  STAPL_TEST_REPORT(
    (std::string("std::tuple<int, int>")==get_type_name<std::tuple<int,int>>()),
    "type_printer<std::tuple<int,int>>");
  auto t = std::forward_as_tuple(42, 42);
  const std::string s = get_type_name<decltype(t)>();
  STAPL_TEST_REPORT(
    (std::string("std::tuple<int&&, int&&>")==s),
    "type_printer<std::forward_as_tuple(42, 42)>");

  // references
  STAPL_TEST_REPORT(
    (std::string("double const&")==get_type_name<double const&>()),
     "type_printer<double const&>");
  STAPL_TEST_REPORT(
    (std::string("double&")==get_type_name<double&>()),
     "type_printer<double&>");
  STAPL_TEST_REPORT(
    (std::string("double&&")==get_type_name<double&&>()),
     "type_printer<double&&>");
}


void test_typelist_printer(void)
{
  STAPL_TEST_REPORT(
    (std::string("int, float const&, double&, char&&") ==
     get_type_names<int, float const&, double&, char&&>()),
     "typelist_printer<int, float const&, double&, char&&>");

  STAPL_TEST_REPORT(
    (std::string("") == get_type_names<>()),
     "typelist_printer<>");
}


void test_object_type_printer(void)
{
  int i { };
  double j { };
  std::tuple<double, bool> k { };

  STAPL_TEST_REPORT(
    (std::string("int, double, std::tuple<double, bool>") ==
     get_type_names(i, j, k)),
     "object_type_printer<int, double, std::tuple<double, bool>>");
}


stapl::exit_code stapl_main(int, char*[])
{
  test_type_printer();
  test_typelist_printer();
  test_object_type_printer();
  return EXIT_SUCCESS;
}
