/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/containers/array/array.hpp>
#include <stapl/views/array_view.hpp>
#include <stapl/algorithms/algorithm.hpp>

#include "../test_report.hpp"

using namespace stapl;

template<typename T>
struct setter
{
private:
  typename T::first_type m_first;
  typename T::second_type m_second;

public:
  typedef T result_type;

  setter()
  { }

  setter(typename T::first_type first, typename T::second_type second)
    : m_first(first), m_second(second)
  { }

  template<typename P>
  result_type operator()(P p)
  {
    p.first = m_first;
    p.second = m_second;
    return p;
  }

  void define_type(typer& t)
  {
    t.member(m_first);
    t.member(m_second);
  }
};


template<typename T>
struct checker
{
  typename T::first_type m_first;
  typename T::second_type m_second;

  typedef bool result_type;

  checker() {}

  checker(typename T::first_type first, typename T::second_type second)
    : m_first(first), m_second(second)
  { }

  template<typename P>
  result_type operator()(P x)
  {
    return x.first == m_first && x.second == m_second;
  }

  void define_type(typer& t)
  {
    t.member(m_first);
    t.member(m_second);
  }
};


stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << "usage: exe n" << std::endl;
    exit(1);
  }

  size_t n = atoi(argv[1]);

  typedef std::pair<long, bool>     pair_type;
  typedef array<pair_type>          array_type;
  typedef array_view<array_type>    view_type;

  array_type a(n);
  view_type v(a);

  map_func(setter<pair_type>(n, true), v);
  size_t count = count_if(v, checker<pair_type>(n, true));

  STAPL_TEST_REPORT(count == n, "Testing specialization of proxy for std::pair")

  return EXIT_SUCCESS;
}
