/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/skeletons/map.hpp>

#include <stapl/containers/array/array.hpp>

#include <stapl/views/array_view.hpp>
#include <stapl/views/repeated_view.hpp>

#include <stapl/algorithms/algorithm.hpp>

using namespace stapl;


struct assign1_wf
{
  typedef void result_type;

  template<typename Ref, typename View>
  void operator()(Ref elem, View v) const
  {
    for (size_t i=0; i<v.size(); ++i)
      elem += v[i];
  }
};


struct assign2_wf
{
  typedef void result_type;

  template<typename View, typename Ref>
  void operator()(View v, Ref elem) const
  {
    for (size_t i=0; i<v.size(); ++i)
      elem += v[i];
  }
};


struct assign3_wf
{
  typedef void result_type;

  template<typename Ref1, typename View1, typename Ref2>
  void operator()(Ref1 elem, View1 v1, Ref2) const
  {
    for (size_t i=0; i<v1.size(); ++i)
      elem += v1[i];
  }
};


struct assign4_wf
{
  typedef void result_type;

  template<typename View1, typename Ref1, typename Ref2>
  void operator()(View1 v1, Ref1 elem, Ref2) const
  {
    for (size_t i=0; i<v1.size(); ++i)
      elem += v1[i];
  }
};


struct assign5_wf
{
  typedef void result_type;

  template<typename View1, typename Ref1, typename Ref2>
  void operator()(Ref1, Ref2 elem, View1 v1) const
  {
    for (size_t i = 0; i < get<0>(v1.dimensions()); ++i)
      for (size_t j = 0; j < get<1>(v1.dimensions()); ++j)
        elem += v1(i,j);
  }
};


struct assign6_wf
{
  typedef void result_type;

  template<typename View1, typename Ref1, typename Ref2>
  void operator()(Ref1 elem, View1 v1, Ref2) const
  {
    for (size_t i = 0; i < get<0>(v1.dimensions()); ++i)
      for (size_t j = 0; j < get<1>(v1.dimensions()); ++j)
        elem += v1(i,j);
  }
};


exit_code stapl_main(int argc, char* argv[])
{
  //
  // array / repeated 1D tests
  //
  do_once([]() { std::cout << "Testing array / repeat_view (1D)..."; });

  typedef array<int>               array_type;
  typedef array_view<array_type>   view_type;

  array_type ar1(100, 1);
  array_type ar2(100, 0);
  array_type ar3(100, 0);
  array_type ar4(100, 0);
  array_type ar5(100, 0);

  view_type v1(ar1);
  view_type v2(ar2);
  view_type v3(ar3);
  view_type v4(ar4);
  view_type v5(ar5);

  map_func(assign1_wf(), v2, make_repeat_view(v1));
  map_func(assign2_wf(), make_repeat_view(v1), v3);
  map_func(assign3_wf(), v4, make_repeat_view(v1), v2);
  map_func(assign4_wf(), make_repeat_view(v1), v5, v2);

  bool ret_val = (count(v2, 100) == 100)
              && (count(v3, 100) == 100)
              && (count(v4, 100) == 100)
              && (count(v5, 100) == 100);


  do_once([ret_val]() {
    if (ret_val)
      std::cout << "Passed\n";
    else
      std::cout << "Fail\n";
  });

  return EXIT_SUCCESS;
}
