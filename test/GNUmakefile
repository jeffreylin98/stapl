# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifndef STAPL
  export STAPL = $(shell echo "$(PWD)" | sed 's,/test,,')
endif

include $(STAPL)/GNUmakefile.STAPLdefaults

.PHONY:compile test clean
default: compile

SUBDIRS:= runtime/type_traits runtime/concurrency runtime/utility \
          runtime/serialization runtime/system runtime runtime/gang \
          runtime/executor runtime/interoperability \
          containers/array containers/consistency \
          containers/composition \
          containers/vector containers/graph \
          views views/partitions views/type_traits views/metadata \
          views/metadata/projection \
          algorithms algorithms/unit \
          skeletons \
          paragraph \
          profiler utility utility/tuple

SUBDIRS_SERIAL:= containers/composition algorithms

# not compiling: views/associated

#
## test related rules
#

TESTDIRS:=$(addsuffix .test, $(SUBDIRS))

.PHONY: test runtests $(TESTDIRS)

test: compile compile_serial
	$(MAKE) -l 0.0 runtests

$(TESTDIRS): %.test: compile
	$(MAKE) -C $* test

runtests: $(TESTDIRS)

#
## compile rules
#

COMPILEDIRS:=$(addsuffix .compile, $(SUBDIRS))
COMPILEDIRS_SERIAL:=$(addsuffix .compile_serial, $(SUBDIRS_SERIAL))

.PHONY: compile compile_serial $(COMPILEDIRS) $(COMPILEDIRS_SERIAL)

compile: $(COMPILEDIRS)
	$(MAKE) -l 0.0 compile_serial

$(COMPILEDIRS): %.compile:
	$(MAKE) -C $* compile

#Special target for memory-heavy compilations, so they are serialized
compile_serial:
	$(MAKE) -l 0.0 $(COMPILEDIRS_SERIAL)

$(COMPILEDIRS_SERIAL): %.compile_serial:
	$(MAKE) -C $* compile_serial

#
## clean related rules
#
SUBCLEAN:= $(addsuffix .clean,$(SUBDIRS))
.PHONY: $(SUBCLEAN)

clean: $(SUBCLEAN)

$(SUBCLEAN): %.clean:
	$(MAKE) -C $* clean
