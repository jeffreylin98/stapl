/*
 // Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
 // component of the Texas A&M University System.

 // All rights reserved.

 // Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STAPL_PROFILING_SIMPLE_PROFILER_HPP
#define STAPL_PROFILING_SIMPLE_PROFILER_HPP

#include <stapl/runtime.hpp>
#include "profiler_util.h"

namespace stapl {

namespace profiling {

template <typename Counter, typename Predicate>
class simple_profiler
  : public base_profiler<Counter, Predicate>
{
private:
  typedef base_profiler<Counter, Predicate> base_type;

protected:
  size_t n_times;
  bool m_time_per_invocation;

public:
  simple_profiler(std::string inname, int argc = 0, char **argv = NULL)
    : base_type(inname, argc, argv), m_time_per_invocation(false)
  {
    n_times = 1;
    for (int i = 1; i < argc; i++) {
      if (!strcmp("--ntimes", argv[i]))
        n_times = atoi(argv[++i]);
    }
  }

  void report(std::string str)
  {
    if (get_location_id() == 0) {
      std::cout << str << "  ";
      if (m_time_per_invocation) {
        std::cout << this->avg / this->m_test_size << " ";
        std::cout << this->confidenceinterval << " ";
        std::cout << this->min / this->m_test_size << " ";
        std::cout << this->max / this->m_test_size << " ";
        std::cout << this->iterations;
      }
      else {
        std::cout << this->avg << " ";
        std::cout << this->confidenceinterval << " ";
        std::cout << this->min << " ";
        std::cout << this->max << " ";
        std::cout << this->iterations;
      }
      print_extra_information<
        typename Counter::value_type, simple_profiler
      >::print(*this);
      std::cout << "\n";
    }
  }
};

} // namespace profiling

} // namespace stapl

#endif
