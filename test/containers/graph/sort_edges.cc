/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#include <stapl/runtime.hpp>
#include <stapl/containers/graph/graph.hpp>
#include <stapl/containers/graph/csr_graph.hpp>
#include <stapl/containers/graph/csr_utils.hpp>
#include <stapl/containers/graph/short_csr_graph.hpp>
#include <stapl/containers/graph/generators/erdos_renyi.hpp>

#include "../../test_report.hpp"

using namespace stapl;

//////////////////////////////////////////////////////////////////////
/// @brief Compare edges by target vertex id in reverse order.
//////////////////////////////////////////////////////////////////////
struct reverse_edge_target_comp
{
  template<typename Edge1, typename Edge2>
  bool operator()(Edge1&& e1, Edge2&& e2)
  {
    return e2.target() < e1.target();
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Check whether edges incident on a vertex are in sorted order
/// according to the given comparator.
//////////////////////////////////////////////////////////////////////
template<typename Comp>
struct check_sorted
{
private:
  Comp m_comp;

public:
  check_sorted() = default;

  check_sorted(Comp&& comp)
  : m_comp(comp)
  { }

  using result_type = bool;
  template<typename Vertex>
  result_type operator()(Vertex&& vertex)
  {
    return std::is_sorted(vertex.begin(), vertex.end(), m_comp);
  }

  void define_type(typer& t)
  {
    t.member(m_comp);
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Test a graph type for working sort_edges methods.
/// @tparam Graph graph type to test
/// @param n size of graph to create
//////////////////////////////////////////////////////////////////////
template<typename Graph>
void test_graph_sort_edges(size_t n)
{
  using view_type = graph_view<Graph>;
  using locality_comp = detail::edge_target_location_comp<Graph>;
  using target_comp = detail::edge_target_comp;
  using reverse_comp = reverse_edge_target_comp;

  auto gv = stapl::generators::make_erdos_renyi<view_type>(n, 0.4, true);
  try_commit(gv.container());

  gv.sort_edges_locality();
  bool sorted_locality = all_of(gv,
    check_sorted<locality_comp>{locality_comp{gv.get_container()}});
  STAPL_TEST_REPORT(sorted_locality, "edges sorted by target locality");

  gv.sort_edges(reverse_edge_target_comp{});
  bool sorted_reverse = all_of(gv, check_sorted<reverse_comp>{});
  STAPL_TEST_REPORT(sorted_reverse, "edges sorted by custom comparator");

  gv.sort_edges_ascending();
  bool sorted_ascending = all_of(gv, check_sorted<target_comp>{});
  STAPL_TEST_REPORT(sorted_ascending, "edges sorted by target vertex id");
}

stapl::exit_code stapl_main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << "usage: exe n" << std::endl;
    exit(1);
  }
  size_t n  = atol(argv[1]) * get_num_locations();

  STAPL_TEST_MESSAGE("Testing sort_edges stapl::graph");
  test_graph_sort_edges<
    stapl::graph<stapl::UNDIRECTED, stapl::NONMULTIEDGES>>(n);

  STAPL_TEST_MESSAGE("Testing sort_edges short csr");
  test_graph_sort_edges<small_short_csr_graph<stapl::UNDIRECTED>>(n);

  return EXIT_SUCCESS;
}
