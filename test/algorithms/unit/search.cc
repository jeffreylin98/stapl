/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <cstdlib>
#include <iostream>
#include <vector>

#include <stapl/containers/array/array.hpp>
#include <stapl/algorithm.hpp>
#include <stapl/views/array_view.hpp>
#include <stapl/views/counting_view.hpp>

#include "../../test_report.hpp"

using namespace stapl;

using timer_type = counter<default_timer>;

stapl::exit_code stapl_main(int argc, char* argv[])
{
  const int n = (argc >= 2) ? atoi(argv[1]) : 100;
  const int m = (argc >= 3) ? atoi(argv[2]) : 5;

  using value_type       = int;
  using array_type       = array<value_type>;
  using view_type        = array_view<array_type>;
  using vector_type      = std::vector<value_type>;
  using vector_view_type = array_view<vector_type>;

  array_type array(n);
  view_type  view(array);

  copy(counting_view<value_type>(n), view);

  //
  // std::vector search string
  //
  vector_type      vec(m);
  vector_view_type vector_view(vec);

  for (int i=0; i<m; ++i)
    vec[i]=n-m+i;

  for (int i = 0; i < 32; ++i)
  {
    timer_type benchmark_timer;
    benchmark_timer.start();

    const auto ref3 = search(view, vector_view);

    const double benchmark_elapsed = benchmark_timer.stop();

    if (ref3 != n-m)
      abort("Failure on search with std::vector");

    stapl::array<double> times_ct(get_num_locations());
    times_ct[get_location_id()] = benchmark_elapsed;
    array_view<stapl::array<double>> times_vw(times_ct);
    double result = reduce(times_vw, max<double>());

    do_once([i, result]()
    {
      std::cout << "search(std::vector) Iteration " << i
                << " time=" << result << "\n";
    });
  }

  STAPL_TEST_REPORT(true, "Testing search over array, string in std::vector")

  //
  // stapl::array search string
  //
  array_type search_str(m);
  view_type  search_str_view(search_str);

  copy(counting_view<value_type>(m, n-m), search_str_view);

  for (int i = 0; i < 32; ++i)
  {
    timer_type benchmark_timer;
    benchmark_timer.start();

    const auto ref3 = search(view, search_str_view);

    const double benchmark_elapsed = benchmark_timer.stop();

    if (ref3 != n-m)
      abort("Failure on search with stapl::array");

    stapl::array<double> times_ct(get_num_locations());
    times_ct[get_location_id()] = benchmark_elapsed;
    array_view<stapl::array<double>> times_vw(times_ct);
    double result = reduce(times_vw, max<double>());

    do_once([i, result]()
    {
      std::cout << "search(stapl::array) Iteration " << i
                << " time=" << result << "\n";
    });
  }

  STAPL_TEST_REPORT(true, "Testing search over array, string in stapl::array")

  return EXIT_SUCCESS;
}
