/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TEST_POINT_H
#define TEST_POINT_H

#include <string>
#include <ostream>
#include <limits>
#include <stapl/runtime.hpp>

namespace stapl {

// class point with x and y coordinates
class Point
{
 public:
  double x;
  double y;

  Point() : x(0.0), y(0.0) { }
  Point(const Point& p) : x(p.x), y(p.y) { }
  Point(const double a, const double b) : x(a), y(b) { }
  Point(const double a) : x(a), y(a) { }
  Point(const int a) : x(a), y(a) { }

  void set_x(double a){ x=a; }
  void set_y(double a){ y=a; }

  void define_type(stapl::typer &t){
    t.member(x);
    t.member(y);
  }

  bool operator<(const Point& p) const {
    return x < p.x;
  }

  bool operator>(const Point& p) const {
    return x > p.x;
  }

  bool operator==(const Point& p) const {
    return (x - p.x < std::numeric_limits<double>::epsilon())
      && (y - p.y < std::numeric_limits<double>::epsilon());
  }

  bool operator!=(const Point& p) const {
    return !(operator==(p));
  }

  // pre-increment
  inline Point& operator++(){
    x += 1.0; y += 1.0;
    return *this;
  }

  // post increment
  inline Point operator++(int) {
    Point tmp = *this;
    x += 1.0; y += 1.0;
    return tmp;
  }

  // pre-decrement
  inline Point& operator--(){
    x -= 1.0; y -= 1.0;
    return *this;
  }

  // post decrement
  inline Point operator--(int) {
    Point tmp = *this;
    x -= 1.0; y -= 1.0;
    return tmp;
  }

  inline Point operator+(const Point& p) const {
    Point c;
    c.x = this->x + p.x;
    c.y = this->y + p.y;
    return c;
  }

  // unary negate
  inline Point operator-(const Point& p) const {
    return Point(-p.x, -p.y);
  }

  // unary negate
  inline void operator-(void) {
    this->x = -this->x;
    this->y = -this->y;
  }

  // unary negate
  inline Point operator-(void) const {
    return Point(-this->x, -this->y);
  }

  void operator+=(const Point& p) {
    this->x += p.x;
    this->y += p.y;
  }
};


inline std::ostream& operator<<(std::ostream& s, Point const& p)
{
  s<<"("<<p.x<<","<<p.y<<")";
  return s;
}


STAPL_DEFINE_IDENTITY_VALUE(plus<Point>,       Point, Point(0, 0))
STAPL_DEFINE_IDENTITY_VALUE(multiplies<Point>, Point, Point(1, 1))

} // namespace stapl

#endif
