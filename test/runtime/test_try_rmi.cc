/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test @ref stapl::try_rmi().
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include "test_utils.h"

using namespace stapl;

class p_test
: public p_object
{
public:
  // test certain execution
  void test_certain(void)
  {
    p_test_object obj(allow_try_rmi);
    try_rmi(obj.get_left_neighbor(), obj.get_rmi_handle(),
            &p_test_object::set, 0, obj.get_location_id());
    int i = sync_rmi(obj.get_left_neighbor(), obj.get_rmi_handle(),
                     &p_test_object::get, 0);
    async_rmi(obj.get_left_neighbor(), obj.get_rmi_handle(),
              &p_test_object::set_sync, 0);
    block_until([&obj] { return obj.get_sync(0); });
    STAPL_RUNTIME_TEST_CHECK(i, int(obj.get_location_id()));
  }

  // test uncertain execution
  void test_uncertain(void)
  {
    p_test_object obj(allow_try_rmi);
    try_rmi(obj.get_left_neighbor(), obj.get_rmi_handle(),
            &p_test_object::set, 0, obj.get_location_id());
  }

  // test always failed execution
  void test_certain_not(void)
  {
    {
      p_test_object obj(allow_try_rmi);
      try_rmi(obj.get_left_neighbor(), obj.get_rmi_handle(),
              &p_test_object::set, 0, obj.get_location_id());
    }
    {
      p_test_object obj(allow_try_rmi);
      rmi_fence(); // quiescence to detect RMIs that should not have executed
      STAPL_RUNTIME_TEST_CHECK(obj.exists(0), false);
    }
  }

  void execute(void)
  {
    test_certain();
    test_uncertain();
    test_certain_not();
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();

#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
