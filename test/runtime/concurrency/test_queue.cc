/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE queue<T>
#include "utility.h"
#include <stapl/runtime/concurrency/queue.hpp>

using stapl::runtime::queue;

// adds to the queue
template<typename T>
struct add_to_queue
{
  stapl::runtime::queue<T>& m_q;
  const unsigned int        m_n;
  const T                   m_t;

  add_to_queue(queue<T>& q,
               const unsigned int n,
               T const& t)
  : m_q(q),
    m_n(n),
    m_t(t)
  { }

  void operator()(void)
  {
    for (unsigned int i = 0; i<m_n; ++i) {
      m_q.push(m_t);
    }
  }
};

BOOST_AUTO_TEST_CASE( queue_writes )
{
  unsigned int NTHREADS = 32;

  queue<int> q;

#if defined(STAPL_RUNTIME_USE_OMP)

# pragma omp parallel
  {
#  pragma omp single
    NTHREADS = omp_get_num_threads();
    int i = omp_get_thread_num();

    add_to_queue<int> a(q, i+1, i);
    a();
  }

#else

  // create some threads
  std::vector<std::thread> threads;
  threads.reserve(NTHREADS);
  for (unsigned int i=0; i<NTHREADS; ++i) {
    threads.emplace_back(add_to_queue<int>(q, i+1, i));
  }

  BOOST_CHECK_EQUAL(NTHREADS, threads.size());

  // wait for them
  for (unsigned int i=0; i<threads.size(); ++i) {
    threads[i].join();
  }

#endif

  std::vector<unsigned int> vals(NTHREADS, 0);
  int x = 0;
  while (q.try_pop(x)) {
    ++vals[x];
  }
  for (unsigned int i=0; i<NTHREADS; ++i) {
    BOOST_CHECK_EQUAL(vals.at(i), (i+1));
  }
}
