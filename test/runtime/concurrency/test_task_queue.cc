/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE task_queue<T>
#include "utility.h"
#include <stapl/runtime/concurrency/task_queue.hpp>
#include <algorithm>
#include <mutex>
#include <vector>
#include <utility>

using namespace stapl::runtime;

template<typename T>
class shared_object
{
private:
  std::vector<std::pair<int, T> >  m_vector;
  task_queue<void(shared_object*)> m_q;
  std::mutex                       m_mtx;

  void add_internal(unsigned int i, T const& t)
  { m_vector.push_back(std::make_pair(i, t)); }

public:
  shared_object(void) = default;

  shared_object(shared_object const&) = delete;
  shared_object& operator=(shared_object const&) = delete;

  void add(unsigned int i, T const& t)
  {
    std::unique_lock<std::mutex> lock(m_mtx, std::try_to_lock);
    if (!lock.owns_lock()) {
      m_q.add([i, t](shared_object* p) { p->add_internal(i, t); });
      return;
    }
    m_q.drain(this);
    add_internal(i, t);
  }

  void done(void)
  {
    std::lock_guard<std::mutex> lock(m_mtx);
    m_q.drain(this);
  }

  std::vector<std::vector<T> > get(const unsigned int size) const
  {
    std::vector<std::vector<T> > v(size);
    for (unsigned int i=0; i<m_vector.size(); ++i) {
      const unsigned int tid = m_vector[i].first;
      BOOST_REQUIRE(tid<v.size());
      v[tid].push_back(m_vector[i].second);
    }
    return v;
  }
};

void do_it(shared_object<int>& o, int i)
{
  const int N  = 100000;
  for (int n = 0; n<N; ++n)
    o.add(i, n);
}

BOOST_AUTO_TEST_CASE( queue_writes )
{
  unsigned int NTHREADS = 32;

  shared_object<int> so;

#if defined(STAPL_RUNTIME_USE_OMP)

# pragma omp parallel shared(so)
  {
#  pragma omp single
    NTHREADS = omp_get_num_threads();
    int i = omp_get_thread_num();

    do_it(so, i);
  }

#else

  // create some threads
  std::vector<std::thread> threads;
  threads.reserve(NTHREADS);
  for (unsigned int i=0; i<NTHREADS; ++i) {
    threads.emplace_back([&so, i] { do_it(so, i); });
  }

  BOOST_CHECK_EQUAL(NTHREADS, threads.size());

  // wait for them
  for (unsigned int i=0; i<threads.size(); ++i) {
    threads[i].join();
  }

#endif

  so.done();

  std::vector<std::vector<int> > vals = so.get(NTHREADS);
  for (unsigned int i=0; i<vals.size(); ++i) {
    BOOST_REQUIRE(std::is_sorted(vals[i].begin(), vals[i].end()));
  }
}
