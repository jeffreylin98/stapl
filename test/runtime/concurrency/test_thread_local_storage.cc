/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE thread_local_storage
#include "utility.h"
#include <stapl/runtime/concurrency/thread_local_storage.hpp>
#include <mutex>
#include <vector>

using namespace stapl::runtime;

struct A
{
  int i;

  A(void) noexcept
  : i(0)
  { }

  void inc(void) noexcept
  { ++i; }
};

static std::mutex mtx;
STAPL_RUNTIME_THREAD_LOCAL(A, tls_a)
STAPL_RUNTIME_THREAD_LOCAL(A, tls_b)

void foo(void)
{
  ++tls_a.get().i;
  ++tls_b.get().i;
  std::lock_guard<std::mutex> l(mtx);
  BOOST_CHECK_EQUAL(tls_a.get().i, 1);
  BOOST_CHECK_EQUAL(tls_b.get().i, 1);
}


BOOST_AUTO_TEST_CASE( thread_local_storage_A )
{
#if defined(STAPL_RUNTIME_USE_OMP)

# pragma omp parallel
  {
    foo();
  }

#else

  const unsigned int NTHREADS = 100;

  // create some threads
  std::vector<std::thread> threads;
  threads.reserve(NTHREADS);
  for (unsigned int i=0; i<NTHREADS; ++i) {
    threads.emplace_back(&foo);
  }

  // wait for them
  for (unsigned int i=0; i<threads.size(); ++i) {
    threads[i].join();
  }

#endif

}
