#!/bin/sh

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

TEST_OPTIONS='--report_level=no --log_level=nothing'
TEST_OUTPUT_PREFIX='(STAPL RTS serialization)'

eval ./test_typer_traits $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_typer_traits - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_typer_traits - [passed]"
fi

eval ./test_boost_serialization $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_boost_serialization - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_boost_serialization - [passed]"
fi

eval ./test_arg_storage $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_arg_storage - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_arg_storage - [passed]"
fi

eval ./test_stl_containers $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_stl_containers - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_stl_containers - [passed]"
fi

eval ./test_boost_containers $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_boost_containers - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_boost_containers - [passed]"
fi

eval ./test_string $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_string - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_string - [passed]"
fi

eval ./test_function $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_function - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_function - [passed]"
fi

eval ./test_smart_ptr $TEST_OPTIONS
if test $? != 0
then
    echo $TEST_OUTPUT_PREFIX" test_smart_ptr - [FAILED]"
else
    echo $TEST_OUTPUT_PREFIX" test_smart_ptr - [passed]"
fi
