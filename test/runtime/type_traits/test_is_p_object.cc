/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#define STAPL_RUNTIME_TEST_MODULE is_p_object
#include "utility.h"
#include <stapl/runtime/type_traits/is_p_object.hpp>
#include <stapl/runtime/p_object.hpp>

using namespace stapl;

class A
: public p_object
{ };


/// is_p_object

BOOST_AUTO_TEST_CASE( test_is_p_object )
{
  typedef p_object T;

  BOOST_CHECK_EQUAL(is_p_object<T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<const T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<volatile T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<const volatile T>::value, true);

  BOOST_CHECK_EQUAL(is_p_object<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object<T const volatile* volatile const>::value, false);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_A )
{
  typedef A T;

  BOOST_CHECK_EQUAL(is_p_object<T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<const T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<volatile T>::value, true);
  BOOST_CHECK_EQUAL(is_p_object<const volatile T>::value, true);

  BOOST_CHECK_EQUAL(is_p_object<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object<T const volatile* volatile const>::value, false);
}

BOOST_AUTO_TEST_CASE( test_is_p_object_int )
{
  typedef int T;

  BOOST_CHECK_EQUAL(is_p_object<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object<T const volatile* volatile const>::value, false);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_int_array )
{
  typedef int T[10];

  BOOST_CHECK_EQUAL(is_p_object<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object<T const volatile* volatile const>::value, false);
}


/// is_p_object_reference

BOOST_AUTO_TEST_CASE( test_is_p_object_reference )
{
  typedef p_object T;

  BOOST_CHECK_EQUAL(is_p_object_reference<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile&>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_reference<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile const>::value, false);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_reference_A )
{
  typedef A T;

  BOOST_CHECK_EQUAL(is_p_object_reference<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile&>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile&>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_reference<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile const>::value, false);
}

BOOST_AUTO_TEST_CASE( test_is_p_object_reference_int )
{
  typedef int T;

  BOOST_CHECK_EQUAL(is_p_object_reference<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile const>::value, false);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_reference_int_array )
{
  typedef int T[10];

  BOOST_CHECK_EQUAL(is_p_object_reference<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_reference<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_reference<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_reference<T const volatile* volatile const>::value, false);
}



/// is_p_object_pointer

BOOST_AUTO_TEST_CASE( test_is_p_object_pointer )
{
  typedef p_object T;

  BOOST_CHECK_EQUAL(is_p_object_pointer<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile*>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile* const>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* volatile>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile const>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T volatile* volatile const>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile const>::value, true);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_pointer_A )
{
  typedef A T;

  BOOST_CHECK_EQUAL(is_p_object_pointer<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile*>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile*>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile* const>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* volatile>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile>::value, true);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile const>::value, true);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile const>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T volatile* volatile const>::value, true);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile const>::value, true);
}

BOOST_AUTO_TEST_CASE( test_is_p_object_pointer_int )
{
  typedef int T;

  BOOST_CHECK_EQUAL(is_p_object_pointer<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile const>::value, false);
}


BOOST_AUTO_TEST_CASE( test_is_p_object_pointer_int_array )
{
  typedef int T[10];

  BOOST_CHECK_EQUAL(is_p_object_pointer<T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<volatile T>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<const volatile T>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile&>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile&>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile*>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile*>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const volatile* const>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T volatile* volatile>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile>::value, false);

  BOOST_CHECK_EQUAL(is_p_object_pointer<T* volatile const>::value, false);
  BOOST_CHECK_EQUAL(is_p_object_pointer<T const* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T volatile* volatile const>::value, false);
  BOOST_CHECK_EQUAL(
    is_p_object_pointer<T const volatile* volatile const>::value, false);
}
