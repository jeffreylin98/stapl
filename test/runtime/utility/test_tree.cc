/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#define STAPL_RUNTIME_TEST_MODULE trees
#include "utility.h"
#include <stapl/runtime/utility/tree.hpp>
#include <algorithm>
#include <vector>

const int N = 32;

using namespace stapl::runtime;

BOOST_AUTO_TEST_CASE( test_flat_tree )
{
  for (int n=1; n<=N; ++n) {
    bool root_found = false;
    std::vector<int> parents(n, -1);

    int count = 0;

    for (int id = 0; id < n; ++id) {
      // generate topology
      const auto r        = make_flat_tree(id, n);
      const auto root     = std::get<0>(r);
      const auto parent   = std::get<1>(r);
      const auto children = std::get<2>(r);

      // check if it is root
      if (id==root) {
        // make sure we have only one root
        BOOST_REQUIRE_EQUAL(root_found, false);
        root_found = true;

        // make sure that the root's parent is the root
        BOOST_REQUIRE_EQUAL(root, parent);

        // count root
        ++count;
      }

      // check if a parent is set if it is the same as the expected
      if (parents[id]!=-1) {
        BOOST_REQUIRE_EQUAL(parents[id], parent);
      }
      else {
        parents[id] = parent;
      }

      // check children
      for (auto const& v : children) {
        // child should not be above total number of children
        BOOST_REQUIRE(v<n);

        // child cannot be root
        BOOST_REQUIRE(v!=root);

        // child cannot be parent
        BOOST_REQUIRE(v!=parent);

        // check if a parent is set if it is the same as the expected
        if (parents[v]!=-1) {
          BOOST_REQUIRE_EQUAL(parents[v], id);
        }
        else {
          parents[v] = id;
        }

        // count child
        ++count;
      }
    }

    // make sure we have one root
    BOOST_REQUIRE_EQUAL(root_found, true);

    // make sure we have all the nodes
    BOOST_REQUIRE_EQUAL(count, n);

    // make sure every node has a parent
    BOOST_REQUIRE(std::find(parents.begin(), parents.end(), -1)==parents.end());
  }
}


BOOST_AUTO_TEST_CASE( test_binary_tree )
{
  for (int n=1; n<=N; ++n) {
    bool root_found = false;
    std::vector<int> parents(n, -1);

    int count = 0;

    for (int id = 0; id < n; ++id) {
      // generate topology
      const auto r        = make_binary_tree(id, n);
      const auto root     = std::get<0>(r);
      const auto parent   = std::get<1>(r);
      const auto children = std::get<2>(r);

      // check if it is root
      if (id==root) {
        // make sure we have only one root
        BOOST_REQUIRE_EQUAL(root_found, false);
        root_found = true;

        // make sure that the root's parent is the root
        BOOST_REQUIRE_EQUAL(root, parent);

        // count root
        ++count;
      }

      // check if a parent is set if it is the same as the expected
      if (parents[id]!=-1) {
        BOOST_REQUIRE_EQUAL(parents[id], parent);
      }
      else {
        parents[id] = parent;
      }

      // check children
      for (auto const& v : children) {
        // child should not be above total number of children
        BOOST_REQUIRE(v<n);

        // child cannot be root
        BOOST_REQUIRE(v!=root);

        // child cannot be parent
        BOOST_REQUIRE(v!=parent);

        // check if a parent is set if it is the same as the expected
        if (parents[v]!=-1) {
          BOOST_REQUIRE_EQUAL(parents[v], id);
        }
        else {
          parents[v] = id;
        }

        // count child
        ++count;
      }
    }

    // make sure we have one root
    BOOST_REQUIRE_EQUAL(root_found, true);

    // make sure we have all the nodes
    BOOST_REQUIRE_EQUAL(count, n);

    // make sure every node has a parent
    BOOST_REQUIRE(std::find(parents.begin(), parents.end(), -1)==parents.end());
  }
}


BOOST_AUTO_TEST_CASE( test_binomial_tree )
{
  for (int n=1; n<=N; ++n) {
    bool root_found = false;
    std::vector<int> parents(n, -1);

    std::cout << "-----> " << N << std::endl;

    int count = 0;

    for (int id = 0; id < n; ++id) {
      // generate topology
      const auto r        = make_binomial_tree(id, n);
      const auto root     = std::get<0>(r);
      const auto parent   = std::get<1>(r);
      const auto children = std::get<2>(r);

      std::cout << id << ": (" << root << "," << parent << ") ( ";
      for (auto&& v : children)
        std::cout << v << ' ';
      std::cout << ')' << std::endl;

      // check if it is root
      if (id==root) {
        // make sure we have only one root
        BOOST_REQUIRE_EQUAL(root_found, false);
        root_found = true;

        // make sure that the root's parent is the root
        BOOST_REQUIRE_EQUAL(root, parent);

        // count root
        ++count;
      }

      // check if a parent is set if it is the same as the expected
      if (parents[id]!=-1) {
        BOOST_REQUIRE_EQUAL(parents[id], parent);
      }
      else {
        parents[id] = parent;
      }

      // check children
      for (auto const& v : children) {
        // child should not be above total number of children
        BOOST_REQUIRE(v<n);

        // child cannot be root
        BOOST_REQUIRE(v!=root);

        // child cannot be parent
        BOOST_REQUIRE(v!=parent);

        // check if a parent is set if it is the same as the expected
        if (parents[v]!=-1) {
          BOOST_REQUIRE_EQUAL(parents[v], id);
        }
        else {
          parents[v] = id;
        }

        // count child
        ++count;
      }
    }

    // make sure we have one root
    BOOST_REQUIRE_EQUAL(root_found, true);

    // make sure we have all the nodes
    BOOST_REQUIRE_EQUAL(count, n);

    // make sure every node has a parent
    BOOST_REQUIRE(std::find(parents.begin(), parents.end(), -1)==parents.end());
  }
}
