/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE block_registry
#include "utility.h"
#include <stapl/runtime/utility/block_registry.hpp>
#include <vector>

using namespace stapl::runtime;

BOOST_AUTO_TEST_CASE( test_block_registry )
{
  const unsigned int id = 3;
  const unsigned int n  = 4;

  block_registry<unsigned int, int> registry(id, n);

  std::vector<int> values;
  std::vector<int> keys;
  for (int i=1; i<20; ++i) {
    values.push_back(i);
  }

  // insert key/data pair
  for (unsigned int i = 0; i<values.size(); ++i) {
    const auto key = registry.reserve_id();
    keys.push_back(key);
    registry.insert(key, values[i]);
  }

  // check if inserted
  for (unsigned int i = 0; i<values.size(); ++i) {
    const int v = registry.retrieve(keys[i]);
    BOOST_CHECK_EQUAL(values[i], v);
  }

  // erase half of them
  for (unsigned int i = 0; i<values.size()/2; ++i) {
    registry.erase(keys[i]);
  }

  // check if gone
  for (unsigned int i = 0; i<values.size()/2; ++i) {
    int v = 0;
    BOOST_CHECK_EQUAL(registry.try_retrieve(keys[i], v), false);
  }

  // check if still there
  for (unsigned int i = values.size()/2; i<values.size(); ++i) {
    const int v = registry.retrieve(keys[i]);
    BOOST_CHECK_EQUAL(values[i], v);
  }
}
